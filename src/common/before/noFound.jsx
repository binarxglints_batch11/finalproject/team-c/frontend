import React from 'react'
import './noFound.scss'

function NotFound() {
    return (
        <div className='not-found'>
            Laundry Not Found
        </div>
    )
}

export default NotFound
