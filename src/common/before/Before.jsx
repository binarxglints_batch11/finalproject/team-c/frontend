import React from 'react';
import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export const Before = () => {
  return (
    <div
      style={{
        // backgroundColor: '#E2E9EE',
        backgroundColor: 'white',
      }}
    >
      <Container
        style={{
          width: '100%',
          height: 600,
          display: 'flex',
          justifyContent: 'center',
          alignItem: 'center',
        }}
      >
        <p
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          Please
          <Link to="/" style={{ textDecoration: 'none' }}>
            &nbsp; login &nbsp;
          </Link>
          to access this page :)
        </p>
      </Container>
    </div>
  );
};
