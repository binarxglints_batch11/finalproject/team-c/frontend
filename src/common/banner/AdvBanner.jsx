import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './AdvBanner.scss';
import AppStore1 from '../../assets/store-appstore.png';
import GoogPlay1 from '../../assets/store-gplay.png';
import Hp1 from '../../assets/adv-smartphone.png';

const AdvBanner = () => {
    return (
    <div className="adv-banner">
    <Container className="banner-container">
        <Row>
            <Col className="hp-ipon-col">
              <img src={Hp1} alt="hp"/>
            </Col>
            <Col className="download-col">
                <h3>Get our mobile App</h3>
                <p>Order laundry services from anywhere</p>
                <p className="avail">Available on</p>
                <div className="icon">
                  <a href="https://apple.com/app-store/">
                    <img src={AppStore1} alt="AppStore" style={{maxWidth: "150px"}}/>
                  </a> 
                  <a href="https://play.google.com">
                    <img src={GoogPlay1} alt="GooglePlay" style={{maxWidth: "150px"}}/>
                  </a>
                  </div>
            </Col>
          
        </Row>        
    </Container>
    </div>

  );
}

export default AdvBanner;
