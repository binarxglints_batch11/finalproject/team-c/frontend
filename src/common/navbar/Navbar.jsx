import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import Cart from './cart/Cart';
import { Container, Nav, Button, Form, FormControl, Navbar } from 'react-bootstrap';
import { BsSearch } from 'react-icons/bs';
import './Navbar.scss';
import { NavLogo } from '../../assets';
import { registerForm, loginForm } from '../../store/actions/authNavbar';
import { GetUserProfile } from '../../store/actions/GetUserProfile';
import { getLaundries } from '../../store/actions/getLaundries';

function NavbarComponent() {
  const [cart, setCart] = useState(false);
  const [searchName, setSearchName] = useState('');
  const dispatch = useDispatch();
  const history = useHistory();
  const token = localStorage.getItem('token');
  const id = localStorage.getItem('id');

  const user = useSelector((state) => state.UserProfile.profile);
  // const loading = useSelector((state) => state.UserProfile.loading);

  useEffect(() => {
    dispatch(GetUserProfile(id));
  }, []);

  const AllLaundry = () => {
    dispatch(getLaundries('all'));
    history.push(`/services/all`);
  };

  const handleSearchInput = (e) => {
    setSearchName(e.target.value);
  };

  const resetSearchInput = () => {
    setSearchName('');
  };

  const submitSearchInput = (event) => {
    dispatch(getLaundries({ name: searchName }));
    event.preventDefault();
    resetSearchInput();
    history.push(`/services/${searchName}`);
  };

  return (
    <Navbar collapseOnSelect expand="lg">
      <Container className="navbar-container">
        <div
          className="logo"
          onClick={() => {
            dispatch(registerForm(false));
            dispatch(loginForm(false));
          }}
        >
          <Link to={'/'}>
            <img className="logo" src={NavLogo} alt="logo-wash-me" />
          </Link>
        </div>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="nav-container">
            <Link className="nav-item nav-link" style={{ color: '#2B495D' }} to="/homepage">
              Home
            </Link>
            <Link className="nav-item nav-link" style={{ color: '#2B495D' }} to="/services/all" onClick={() => AllLaundry()}>
              Services
            </Link>
            <Link className="nav-item nav-link" style={{ color: '#2B495D' }} to="/about-us">
              About Us
            </Link>
            <Link className="nav-item nav-link" style={{ color: '#2B495D' }} to="/contact">
              Contact
            </Link>
          </Nav>
          {token ? (
            <>
              <Form role="search" className="button-container-after-login" onSubmit={submitSearchInput}>
                <FormControl className="input-search" type="text" placeholder="Type laundry name" value={searchName} onChange={handleSearchInput} variant="outline-light" />
                <Button className="btn-search" variant="outline-light" onClick={submitSearchInput}>
                  <BsSearch />
                </Button>
              </Form>
              <div className="user" onClick={() => setCart(!cart)}>
                <Cart />
              </div>
              {/* <div className="user">
								<i className="profile">
									<img
										className="icon-list"
										src={ListOrder}
										alt="user-pic-order-list"
									/>
								</i>
							</div> */}

              <div className="user">
                <a href={`/profile/${user.id}`}>
                  <i className="profile">
                    <img className="icon-ava" src={`https://washme.gabatch11.my.id${user.image}`} alt="pic" />
                  </i>
                </a>
              </div>
            </>
          ) : (
            <div className="button-container">
              {/* {register ? 'ok' : null} */}
              <Button
                // setRegister={setRegister}
                className="btn-item"
                style={{
                  borderColor: '#FF6C6C',
                  color: '#FF6C6C',
                }}
                variant="outline-light"
                onClick={() => {
                  // setRegister(!register);
                  dispatch(registerForm(true));
                }}
              >
                Sign Up
              </Button>
              {/* {login ? 'oke banget' : null} */}
              <Button
                className="btn-item"
                style={{
                  backgroundColor: '#FF6C6C',
                  borderColor: '#FF6C6C',
                  color: '#FFFFFF',
                }}
                variant="light"
                onClick={() => {
                  // setLogin(!login);
                  dispatch(loginForm(true));
                }}
              >
                Log In
              </Button>
            </div>
          )}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default NavbarComponent;
