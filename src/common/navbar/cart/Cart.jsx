import React, { useState, useEffect, useRef, useContext } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import {
	Overlay,
	Popover,
	Button,
	Card,
	ListGroup,
	Accordion,
	AccordionContext,
	useAccordionToggle,
} from "react-bootstrap";
import "./Cart.scss";
import { EmptyCart, WashIron, ListOrder } from "../../../assets/index.js";
import {
	BsArrowRightShort,
	BsChevronDown,
	BsChevronRight,
} from "react-icons/bs";
import { AiOutlineClose } from "react-icons/ai";
import { GetOrder } from "../../../store/actions/getOrder";
import LoadingCard from "../../../common/spinner/loadingCard";

const ContextTerm = ({ eventKey, callback }) => {
	const currentEventKey = useContext(AccordionContext);

	const handleArrow = useAccordionToggle(
		eventKey,
		() => callback && callback(eventKey)
	);

	const isCurrentEventKey = currentEventKey === eventKey;

	return (
		<a onClick={handleArrow}>
			<div>
				{isCurrentEventKey ? (
					<BsChevronDown className="btn-arrow-detail" onClick={handleArrow} />
				) : (
					<BsChevronRight className="btn-arrow-detail" onClick={handleArrow} />
				)}
			</div>
		</a>
	);
};

const Cart = () => {
	const dispatch = useDispatch();
	const orderList = useSelector((state) => state.OrderDetail.userOrder);
	const loading = useSelector((state) => state.OrderDetail.loading);
	const [show, setShow] = useState(false);
	const [item, setItem] = useState(true);
	const [target, setTarget] = useState(null);
	const ref = useRef(null);

    const id = localStorage.getItem('id');

	const handleClick = (event) => {
		setShow(!show);
		setTarget(event.target);
	};
	const handleClose = () => setShow(false);

	const history = useHistory();

	useEffect(() => {
		dispatch(GetOrder());
	}, []);

	const orderDate = (estimateFinish) => {
		let days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
		let d = new Date(estimateFinish);

		let dayName = days[d.getDay()];
		let monthDetail = d.toString().split(" ")[1];
		let dateDay = d.toString().split(" ")[2];

		// console.log("dayName", dayName);
		// console.log("d", d);
		// console.log(typeof orderMonth);

		return (
			<span>{`This order will finish in on ${dayName}, ${monthDetail} ${dateDay}`}</span>
		);
	};

	return (
		<div ref={ref}>
			<a className="user" onClick={handleClick}>
				<i className="profile">
					<img
						className="icon-list"
						src={ListOrder}
						alt="user-pic-order-list"
					/>
				</i>
			</a>

			<Overlay
				show={show}
				target={target}
				placement="bottom"
				container={ref.current}
				containerPadding={20}
			>
				<Popover className="popover-contained" style={{ margin: "27px" }}>
					<Popover.Title as="h3" className="popover-title-h3">
						Your Order{" "}
						<span onClick={handleClose}>
							<AiOutlineClose className="btn-x" style={{ color: "white" }} />
						</span>
					</Popover.Title>
					<Popover.Content>
						<div className="cont-1">
							<div className="popover-item-1">
								<h4 className="title-h4">On process</h4>
								<Link to={`/profile/${id}`}>
									<p
										className="blue-lighter-big-p"
										onClick={() => console.log("clicked-p")}
									>
										See all orders
									</p>
								</Link>
							</div>
                            
							{loading ? (
								'loading'
							) : orderList.length ? (
								orderList?.map((x, index) => {
								return (<Card key={index} className="laundry-card">
										<ListGroup.Item className="card-item-1">
											<div className="item-col-1">
												<p>{x.laundry.name}</p>
												<div className="order-id">
													<p>Order ID</p>
													<p className="grey-p">#{x.id?.slice(0, 8)}</p>
												</div>
												<p className="blue-lighter-small-p">
													{orderDate(x.estimateFinish)}
												</p>
											</div>
                                            <Link to={`/profile/${id}`}>
											<div className="item-col-2">
												<BsArrowRightShort
													className="btn-arrow-next"
													// onClick={() => console.log("clicked")}
												/>
											</div>
                                            </Link>
										</ListGroup.Item>

										<ListGroup.Item className="card-item-2">
											<div className="item-col-1">
												<p>Service</p>
												<p>Weight</p>
												<p>Status</p>
											</div>

											<div className="item-col-2">
												<p className="grey-p">{x.servicelist.services}</p>
												<p className="grey-p">
													{x.weight > 0 ? `${x.weight} kilograms` : "-"}
												</p>
												<p className="grey-p">{x.orderstatus.status}</p>
											</div>
										</ListGroup.Item>
									</Card>
                                    )}).slice(0, 2)
							) : (
								<div classname="popover-content-col-empty">
									<img
                                    src={EmptyCart} 
                                    alt="cart" />
									<h5>You don't have any orders yet</h5>
								</div>
							)}
						</div>

						{/* <div className="line-1">
								<br />
								<hr />
								<br />
							</div>

							<div className="cont-2">
								<div className="popover-item-1">
									<h4 className="title-h4">Your List</h4>
								</div>

								<Card className="laundry-list-card">
									<ListGroup.Item className="card-item-1">
										<div className="item-col-1">
											<h5>ABC Laundry</h5>
										</div>

										<div className="item-col-2">
											<p>Delete</p>
										</div>
									</ListGroup.Item>

									<ListGroup.Item className="card-item-2">
										<div className="item-col-1">
											<img src={WashIron} alt="washiron" />
										</div>

										<div className="item-col-2">
											<h6>Wash & Iron Basic</h6>
											<p id="grey">Weight: 3 kilogram</p>
										</div>

										<div className="item-col-3">
											<h6>Rp 90.000</h6>
											<Accordion>
												<div>
													<ContextTerm eventKey="1" />
												</div>
											</Accordion>
										</div>
									</ListGroup.Item>
								</Card>

								<Button className="btn-order">Continue Order</Button>
							</div>

							<div className="line-2">
								<br />
								<hr />
								<br />
							</div>

							<div className="cont-3">
								<Card className="laundry-list-2-card">
									<ListGroup.Item className="card-item-1">
										<div className="item-col-1">
											<h5>Cloe Laundry</h5>
										</div>

										<div className="item-col-2">
											<p>Delete</p>
										</div>
									</ListGroup.Item>

									<ListGroup.Item className="card-item-2">
										<div className="item-col-1">
											<img src={WashIron} alt="washiron" />
										</div>

										<div className="item-col-2">
											<h6>Wash & Iron Basic</h6>
											<p id="grey">Weight: 3 kilogram</p>
										</div>

										<div className="item-col-3">
											<h6>Rp 90.000</h6>
											<Accordion>
												<div>
													<ContextTerm eventKey="1" />
												</div>
											</Accordion>
										</div>
									</ListGroup.Item>
								</Card>

								<Button className="btn-order">Continue Order</Button>
							</div> */}
					</Popover.Content>
				</Popover>
				
				
			</Overlay>
		</div>
	);
};

export default Cart;
