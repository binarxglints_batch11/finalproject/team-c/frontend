import React from "react";
import './loading.scss'
import {
	Spinner
} from "react-bootstrap";

function LoadingComponent() {
	return (
		<div className="loading">
			<Spinner animation="border" role="status">
				<span className="sr-only">Loading...</span>
			</Spinner>
		</div>
	);
}

export default LoadingComponent;
