import React from "react";
import './loading.scss'
import {
	Spinner
} from "react-bootstrap";

function LoadingCard() {
	return (
		<div className="loading-card">
			<Spinner animation="border" role="status">
				<span className="sr-only">Loading...</span>
			</Spinner>
		</div>
	);
}

export default LoadingCard;
