import './Select.scss';
import { Checkbox } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import Select, { components } from 'react-select';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { getLaundries } from '../../../store/actions/getLaundries';

const MyOption = (props) => {
  const { data } = props;
  return (
    <components.Option {...props} className="select">
      <div className="select-container">
        <div className="select-wrapper">
          <img className="select-img" src={data.image} alt="icon..." />
          <span className="select-options">{data.label}</span>
        </div>
        <Checkbox checked={props.isSelected} />
      </div>
    </components.Option>
  );
};

const WashValueContainer = (props) => {
  const { getValue, hasValue, children } = props;
  const value = getValue();
  if (!hasValue) {
    return <components.ValueContainer {...props}>{children}</components.ValueContainer>;
  }
  return (
    <components.ValueContainer {...props}>
      <div>
        <img style={{ width: '2rem' }} src={value[0].image} alt="icon..." />
        <span>
          {value.length} Service{value.length > 1 ? 's' : ''}
        </span>
      </div>
    </components.ValueContainer>
  );
};

const SelectComponent = () => {
  const [value, setValue] = useState(null);
  const [options, setOptions] = useState([]);
  const token = localStorage.getItem('token');

  const dispatch = useDispatch();
  useSelector((state) => state.laundries.allLaundry);

  const getAllList = async () => {
    const lists = await axios.get('https://washme.gabatch11.my.id/services', {
      headers: {
        Authorization: token,
      },
    });

    const allServices = lists.data.data.map((list) => {
      return {
        label: list.services,
        value: list.id,
        image: list.imageBasic,
      };
    });

    setOptions(allServices);
  };

  useEffect(() => {
    getAllList();
  }, []);

  const multipleSelect = (filter) => {
    dispatch(
      getLaundries({
        page: 1,
        service: filter
          ?.map((select) => {
            return select.value;
          })
          .join(),
      })
    );
    setValue(filter);
  };

  return (
    <Select
      // menuIsOpen
      value={value}
      // setiap onchange panggil api yang ada multiple select
      onChange={multipleSelect}
      options={options}
      components={{ Option: MyOption, ValueContainer: WashValueContainer }}
      isMulti
      hideSelectedOptions={false}
      closeMenuOnSelect={false}
      placeholder="Select by Service"
    />
  );
};

export default SelectComponent;
