import './Select.scss';
import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import { useSelector, useDispatch } from 'react-redux';
import { getLaundries } from '../../../store/actions/getLaundries';

const SelectTypes = () => {
  const [types, setTypes] = useState('');
  const [options, setOptions] = useState([]);
  const token = localStorage.getItem('token');

  const dispatch = useDispatch();
  useSelector((state) => state.laundries.allLaundry);

  const getAllTypes = async () => {
    const types = await axios.get('https://washme.gabatch11.my.id/types', {
      headers: {
        Authorization: token,
      },
    });

    const AllTypes = types.data.data.map((list) => {
      return {
        label: list.types,
        value: list.id,
      };
    });

    setOptions(AllTypes);
  };

  useEffect(() => {
    getAllTypes();
  }, []);

  const serviceTypes = (type) => {
    dispatch(
      getLaundries({
        page: 1,
        type: type.value,
      })
    );
    setTypes(type);
  };

  return (
    <div>
      <Select
        // menuIsOpen
        options={options}
        onChange={serviceTypes}
        defaultValue={types[1]}
        placeholder="Select by Types"
      />
    </div>
  );
};

export default SelectTypes;
