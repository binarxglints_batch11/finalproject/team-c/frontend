import React from 'react';
import './FilterBar.scss';
import { Container } from 'react-bootstrap';

import Dropdown from './dropdown/Dropdown';
import Types from './dropdown/Types';

const FilterBar = () => {
  return (
    <>
      <Container className="filter-bar">
        <div className="filter-wrapper">
          <div className="filter-services">
            <Dropdown />
          </div>
          <div className="filter-types">
            <Types />
          </div>
        </div>
      </Container>
    </>
  );
};

export default FilterBar;
