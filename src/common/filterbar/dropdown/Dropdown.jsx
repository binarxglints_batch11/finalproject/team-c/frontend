import React from 'react';
import { Container } from '@material-ui/core';
import SelectComponent from '../select/Select';
import './Dropdown.scss';

const Dropdown = () => {
  return (
    <div className="select-service">
      <Container style={{ width: '20rem' }}>
        <SelectComponent />
      </Container>
    </div>
  );
};

export default Dropdown;
