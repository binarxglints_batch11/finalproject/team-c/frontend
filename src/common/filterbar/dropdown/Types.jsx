import React from 'react';
import { Container } from '@material-ui/core';
import SelectTypes from '../select/SelectTypes';
import './Types.scss';

function onChangeInput(value) {}

const Types = () => {
  return (
    <Container style={{ width: '20rem' }}>
      <SelectTypes onChange={onChangeInput} />
    </Container>
  );
};

export default Types;
