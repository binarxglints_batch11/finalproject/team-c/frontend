import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './Footer.scss';

import Logo1 from '../../assets/footer-logo.png';

const Footer = () => {
  return (
    <div className="footer">
      <Container className="footer-container">
        <Row className="row-footer">
          <Col className="logo-col col">
            <img src={Logo1} alt="logo" />
          </Col>
          <Col className="menu-col col">
            <h4>Menu</h4>
            <div className="menu-p">
              <div className="menu-1">
                <a href="https://google.com/">
                  <p>Our Services</p>
                </a>
                <a href="https://google.com/">
                  <p>Our Features</p>
                </a>
                <a href="https://google.com/">
                  <p>Search Laundry</p>
                </a>
              </div>
              <div className="menu-2">
                <a href="https://google.com/">
                  <p>Search Service</p>
                </a>
                <a href="hhttps://google.com/">
                  <p>About Us</p>
                </a>
                <a href="https://google.com/">
                  <p>Terms & Condition</p>
                </a>
              </div>
            </div>
          </Col>
          <Col className="office-col col">
            <h4>Our Office</h4>
            <p>Jl. Hayam wuruk no 88, West Jakarta</p>
          </Col>
          <Col className="contact-col col">
            <h4>Contact Us</h4>
            <a href="https://gmail.com/">
              <p>E-mail : help@washme.co.id</p>
            </a>
            <a href="https://instagram.com/">
              <p>Instagram : @Washme.id</p>
            </a>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Footer;
