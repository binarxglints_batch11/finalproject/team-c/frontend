import React from 'react';
import './Contact.scss';
import NavbarComponent from '../../common/navbar/Navbar';

const Contact = () => {
  return (
    <div className="contact-page">
      <NavbarComponent />
      <div className="coming-soon">
        <h1>Coming Soon</h1>
      </div>
    </div>
  );
};

export default Contact;
