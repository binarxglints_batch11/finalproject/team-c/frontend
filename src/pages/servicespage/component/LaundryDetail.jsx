import React from "react";
import { useSelector } from "react-redux";
import "./LaundryDetail.scss";
import { Link } from "react-router-dom";
import { DelivBike } from "../../../assets/index.js";
import { MdStar } from "react-icons/md";

const LaundryDetail = ({ show }) => {
	const detail = useSelector((state) => state.DataLaundry.dataLaundry);

	return (
		<div className={`laundry-detail-card ${show ? "show" : ""}`}>
			<div className="laundry-detail-1">
				<div className="laundry-detail-contents">
					<img
						className="laundry-detail-img"
						src={`https://washme.gabatch11.my.id${detail?.image}`}
						alt="washing..."
					/>

					<div className="contents">
						<div>
							<p className="laundry-detail-name">{detail?.name}</p>
							<p className="laundry-detail-address">
								{detail?.address?.street}
							</p>
							<p className="laundry-detail-services">
								{detail?.total_services} Services
							</p>
							<p className="laundry-detail-courier">
								{detail?.pickUpAndDelivery ? "Pick up & Delivery" : null}
								{detail?.pickUpAndDelivery ? (
									<img
										className="laundry-icon"
										src={DelivBike}
										alt="Courier..."
									/>
								) : null}
							</p>
						</div>
						<div>
							<p className="laundry-rating">
								<MdStar style={{ color: "#FFC107" }} /> {detail?.average_rating}{" "}
								({detail?.total_review} reviews)
							</p>
							<p className="laundry-hours">Open 09.00 am - 09.00 pm</p>
							<Link to={`/laundry/${detail?.id}`}>
								<button className="btn-services">See all services</button>
							</Link>
						</div>
					</div>
				</div>
			</div>
			{detail?.services
				?.map((item, index) => {
					return (
						<div key={index}>
							<div>
								{item?.basic ? (
									<div className="laundry-detail-2">
										<i className="icon-service">
											<img
												className="laundry-detail-services"
												src={`https://washme.gabatch11.my.id/${item.basic.image}`}
												alt="washing..."
											/>
										</i>

										<div className="laundry-detail-services-contents-left">
											<p className="laundry-detail-types">
												{item.name} {item.basic.name}
											</p>
											{item?.basic?.byItem ? (
												<p className="laundry-detail-prices">
													Rp {item.basic.byItem.minimumPrice}/item
												</p>
											) : null}
											{item?.basic?.byWeight ? (
												<p className="laundry-detail-prices">
													Rp {item.basic.byWeight.price}/kilogram
												</p>
											) : null}
										</div>
										<div className="laundry-detail-services-contents-right">
											<p className="laundry-detail-days">
												{item.basic.days} days working
											</p>
											<Link
												to={{
													pathname: `/laundry/${detail?.id}/order/${item.id}/${item.basic.id}`,
													state: {
														id: detail.id,
														laundry: detail.name,
														serviceId: item.id,
														service: item.basic,
														name: item.name,
													},
												}}
											>
												<button className="btn-detail-order">Order</button>
											</Link>
										</div>
									</div>
								) : null}
							</div>
							<div>
								{item?.express ? (
									<div className="laundry-detail-3">
										<i className="icon-service">
											<img
												className="laundry-detail-services"
												src={`https://washme.gabatch11.my.id/${item.express.image}`}
												alt="washing..."
											/>
										</i>
										<div className="laundry-detail-services-contents-left">
											<p className="laundry-detail-types">
												{" "}
												{item.name} {item.express.name}
											</p>
											{item?.express?.byItem ? (
												<p className="laundry-detail-prices">
													Rp {item.express.byItem.minimumPrice}/item
												</p>
											) : null}
											{item?.basic?.byWeight ? (
												<p className="laundry-detail-prices">
													Rp {item.express.byWeight.price}/kilogram
												</p>
											) : null}
										</div>
										<div className="laundry-detail-services-contents-right">
											<p className="laundry-detail-days">
												{item.express.days} days working
											</p>
											<Link
												to={{
													pathname: `/laundry/${detail?.id}/order/${item.id}/${item.express.id}`,
													state: {
														id: detail.id,
														laundry: detail.name,
														serviceId: item.id,
														service: item.express,
														name: item.name,
													},
												}}
											>
												<button className="btn-detail-order">Order</button>
											</Link>
										</div>
									</div>
								) : null}
							</div>
						</div>
					);
				})
				.slice(0, 1)}

			<div className="laundry-detail-btn">
				<Link to={`/laundry/${detail?.id}`}>
					<button className="btn-tnc">
						Read {detail?.name} terms & conditions
					</button>
				</Link>
			</div>
		</div>
	);
};

export default LaundryDetail;
