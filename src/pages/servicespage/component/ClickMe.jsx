import React from 'react';
import { VscArrowLeft } from 'react-icons/vsc';

function ClickMe() {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        border: '1px solid transparent',
        paddingTop: '5rem',
        paddingBottom: '5rem',
        width: '30vmax',
      }}
    >
      <div>
        <VscArrowLeft /> &nbsp; Click me to see more detail
      </div>
    </div>
  );
}

export default ClickMe;
