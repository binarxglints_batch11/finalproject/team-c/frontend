import { React, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import './LaundryList.scss';
import { Container } from 'react-bootstrap';
import { DelivBike } from '../../../assets/index.js';
import LaundryDetail from './LaundryDetail';
import { MdStar } from 'react-icons/md';
import Pagination from '@material-ui/lab/Pagination';
import { useSelector, useDispatch } from 'react-redux';
import { GetDataLaundry } from '../../../store/actions/getDataLaundry';
import { getLaundries } from '../../../store/actions/getLaundries';
import LoadingComponent from '../../../common/spinner/loading';
import NotFound from '../../../common/before/noFound';
import ClickMe from './ClickMe';
import ClickMeUp from './ClickMeup';

const LaundryList = () => {
  const [show, setShow] = useState('');
  const [page, setPage] = useState(1);
  const [click, setClick] = useState(null);

  let { id } = useParams();

  const dispatch = useDispatch();
  const laundries = useSelector((state) => state.laundries.allLaundry);

  const loading = useSelector((state) => state.laundries.loading);

  useEffect(() => {
    dispatch(
      getLaundries({
        service: id === 'all' ? '' : id,

        page: 1,
      })
    );
  }, [id]);

  const detail = (id) => {
    dispatch(GetDataLaundry(id));
    setShow(!show);
    setClick(id);
  };

  const fetchLaundry = (e, value) => {
    const page = value;

    dispatch(
      getLaundries({
        name: id === 'all' ? '' : id,
        page,
      })
    );
    setPage(value);
  };

  return (
    <div>
      <Container className="laundry-container">
        <div
          className="laundry-wrapper"
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
            margin: 0,
          }}
        >
          {loading ? (
            <LoadingComponent />
          ) : laundries.data.length ? (
            laundries.data.map((item, index) => {
              return (
                <>
                  <button key={index} className="laundry-list" onClick={() => detail(item.id)}>
                    <div className="laundry-list-detail">
                      <div className="laundry-contents">
                        <img className="laundry-img" src={`https://washme.gabatch11.my.id${item.image}`} alt="washing..." />
                      </div>
                      <div className="contents">
                        <div className="laundry-contents">
                          <p className="laundry-name">{item.name}</p>
                          <p className="laundry-address">{item.address.street}</p>
                          <p className="laundry-services">{item.total_services} services</p>
                          <p className="laundry-courier">
                            {item?.pickUpAndDelivery ? <img className="laundry-icon" src={DelivBike} alt="Courier..." /> : null}
                            {item?.pickUpAndDelivery ? 'Pick up & Delivery' : null}
                          </p>
                        </div>
                        <div className="laundry-contents-right">
                          <p className="laundry-rating">
                            <MdStar style={{ color: '#FFC107' }} />
                            {item.average_rating}
                          </p>
                          {item?.by_weight ? <p className="laundry-type">By kilogram</p> : null}
                          {item?.by_item ? <p className="laundry-type">By item</p> : null}
                        </div>
                      </div>
                    </div>
                  </button>
                  {click === item.id && <div className="detail-mobile">{show ? <LaundryDetail show={show} /> : <ClickMeUp />}</div>}
                </>
              );
            })
          ) : (
            <NotFound />
          )}
        </div>

        <div className="detail-desktop">{show ? <LaundryDetail show={show} /> : <ClickMe />}</div>
      </Container>

      <div className="pagination">
        <Pagination count={laundries.total_page} page={page} onChange={fetchLaundry} />
      </div>
    </div>
  );
};

export default LaundryList;
