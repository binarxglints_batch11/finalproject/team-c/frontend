import React from 'react';
import { VscArrowUp } from 'react-icons/vsc';

function ClickMeUp() {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        border: '1px solid transparent',
        // marginTop: '1rem',
      }}
    >
      <div
      // style={{
      //   display: 'flex',
      //   justifyContent: 'center',
      //   alignItems: 'center',
      // }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <VscArrowUp />
        </div>

        <p>Click me to see more detail</p>
      </div>
    </div>
  );
}

export default ClickMeUp;
