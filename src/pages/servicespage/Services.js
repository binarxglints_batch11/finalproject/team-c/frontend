import React, { useState } from 'react';
import './Service.scss';
import FilterBar from '../../common/filterbar/FilterBar';
import Footer from '../../common/footer/Footer';
import NavbarComponent from '../../common/navbar/Navbar';
import LaundryList from './component/LaundryList';
import { Before } from '../../common/before/Before';

const Services = () => {
  const token = localStorage.getItem('token');
  const [byName, setByName] = useState('');

  return (
    <div className="servicepage">
      <NavbarComponent setByName={setByName} />
      {token ? (
        <>
          <FilterBar />
          <LaundryList byName={byName} />
        </>
      ) : (
        <Before />
      )}
      <Footer />
    </div>
  );
};

export default Services;
