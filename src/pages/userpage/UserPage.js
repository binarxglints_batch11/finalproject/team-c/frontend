import React from 'react';
import NavbarComponent from '../../common/navbar/Navbar';
import './UserPage.scss';
import { Container } from 'react-bootstrap';
import ProfileCard from './components/ProfileCard';
import ProcessCard from './components/ProcessCard';
import AdvBanner from '../../common/banner/AdvBanner';
import Footer from '../../common/footer/Footer';

function UserPage() {
  return (
    <div className="user-page">
      <NavbarComponent />
      <Container className="user-page-container">
        <div className="profile-user">
          <ProfileCard />
          <div className="card-line"></div>
          <ProcessCard />
        </div>
      </Container>
      <AdvBanner />
      <Footer />
    </div>
  );
}

export default UserPage;
