import React, { useEffect } from 'react';
import { AiFillStar } from 'react-icons/ai';
import './SavedLaundry.scss';
import { Row, Col, Card } from 'react-bootstrap';
import { WashMach, DelivBike } from '../../../assets/index.js';
import { GetFavoriteLaundry } from '../../../store/actions/getFavoriteLaundry';
import { useDispatch, useSelector } from 'react-redux';

function SavedLaundry() {
	const dispatch = useDispatch();
	const fav = useSelector((state) => state.FavoriteLaundry.favLaundry);

	useEffect(() => {
		dispatch(GetFavoriteLaundry());
	}, []);

	return (
		<div className="saved-container">
			{fav?.map((favo, index) => {
				return (
					<div key={index}>
						{favo?.id ?
						<p className="title-saved-container">Saved Laundry</p>
						:
						null}
						<Card className="saved-laundry">
							<Row className="row-list-detail-card">
								<Col className="col-list-detail-card-flex">
									<Card.Img variant="top" className="laundry-pic-img" src={WashMach} />
									<div className="text-card">
										<div className="card-title-name">
											<Card.Title>{favo?.laundry?.name}</Card.Title>
										</div>
										<div className="laundry-loc">
											<p>{favo?.laundry?.address?.street}</p>
											{/* <p className="loc-distance">0.7 km</p> */}
										</div>
										<div className="laundry-services">
											<p>14 services</p>
										</div>
										<div className="laundry-delivery">
											<img className="pic-bike" src={DelivBike} alt="delivery logo" />
											<p>{favo?.laundry?.pickUpAndDelivery ? 'Pick up & Delivery' : null}</p>
										</div>
									</div>
								</Col>
								<Col className="col-list-detail-card">
									<div className="text-card-rating">
										<div className="rating-text">
											<AiFillStar className="rating-star-item" />
											<p>5.0</p>
										</div>
									</div>
									<div className="text-card-rating">
										<div className="text-typeKG">
											<p>{favo?.laundry?.by_weight ? 'By Kilogram' : null}</p>
										</div>
									</div>
									<div className="text-card-rating">
										<div className="text-typeItem">
											<p>{favo?.laundry?.by_item ? 'By Item' : null}</p>
										</div>
									</div>
								</Col>
							</Row>
						</Card>
					</div>
				);
			})}
		</div>
	);
}

export default SavedLaundry;
