import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './EditProfile.scss';
import { Modal, Button, Form, Row, Col } from 'react-bootstrap';
import { SettingAcc } from '../../../assets/index.js';
import { FaUser, FaEnvelope, FaKey, FaPhoneAlt } from 'react-icons/fa';
import { AiOutlineClose } from 'react-icons/ai';
import { PutUserProfile } from "../../../store/actions/PutUserProfile";
import { GetUserProfile } from "../../../store/actions/GetUserProfile";

import { ToastContainer, toast } from "react-toastify";

const EditProfileModal = (props) => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.UserProfile.profile);
  const idUser = localStorage.getItem('id');

  useEffect(() => {
    GetUserProfile(idUser);
  }, []);

  const [editProfile, setEditProfile] = useState({
    name: '',
    mobile_phone: '',
    // "email": "",
    // "password": "",
  });

  const changeInput = (e) => {
    setEditProfile({
      ...editProfile,
      [e.target.name]: e.target.value,
    });
  };


    const submitEdit = (e) => {
        // e.preventDefault();
        sendData();
        // props.onHide;
    }

    const sendData = async () => {
        let formData = new FormData();
        formData.append('name', editProfile.name);
        formData.append('mobile_phone', editProfile.mobile_phone);
        // formData.append('email', editProfile.email);
        // formData.append('password', editProfile.password);
        await dispatch(PutUserProfile(idUser, formData));
    }

    // console.log("edit profile", editProfile);
    // console.log("props", props);
    // console.log("user12", user);


      function notify() {
        toast("🦄 Please refresh your page after edit your profile", {
			position: "top-right",
			autoClose: 5000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
		});
    }

      
      
    return (
        <div className="edit-profile-container">
                <Modal
                    {...props}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    style={{ color: "#2b495d" }}
                >
                    <Modal.Header >
                        <Modal.Title as={Row} className="user-setting" id="contained-modal-title-vcenter">
                            {/* <img
                                className="icon-setting"
                                src={SettingAcc}
                                alt="setting"
                            /> */}
                            <div
                                className="title-div"
                                style={{
                                    justifyContent: "space-between",
                                    display: "flex",
                                    margin: "0 1rem"
                                }}
                            >
                                <h5>Account Setting</h5>
                                <span
                                    style={{
                                        float: "right",
                                        marginLeft: "37.2rem"
                                    }}
                                    onClick={props.onHide}
                                >
                                    <AiOutlineClose className="btn-x" style={{ color: "#2b495d" }} />
                                </span>
                            </div>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>

                            <Form.Group as={Row} controlId="formPlaintextName">
                                <Form.Label column sm="1">
                                    <FaUser style={{ width: "30px", height: "30px" }} />
                                </Form.Label>
                                <Col sm="11">
                                    <Form.Control
                                        type="name"
                                        defaultValue={props?.data?.name}
                                        placeholder="Your Name"
                                        name="name"
                                        onChange={changeInput} />
                                </Col>
                            </Form.Group>

                            {/* <Form.Group as={Row} controlId="formPlaintextEmail">
                            <Form.Label column sm="1">
                                <FaEnvelope style={{width:"30px", height:"30px"}}/>
                            </Form.Label>
                            <Col sm="11">
                                <Form.Control
                                    type="email"
                                    defaultValue={props?.data?.email}
                                    placeholder="email@email.com"
                                    name="email"
                                    onChange={changeInput} />
                            </Col>
                        </Form.Group> */}

                            {/* <Form.Group as={Row} controlId="formPlaintextPassword">
                            <Form.Label column sm="1">
                                <FaKey style={{width:"30px", height:"30px"}}/>
                            </Form.Label>
                            <Col sm="11">
                                <Form.Control
                                    type="password"
                                    defaultValue={props?.data?.password}
                                    placeholder="Password"
                                    name="password"
                                    onChange={changeInput} />
                                <Button className="pass-btn" variant="outline-light">
                                    Change Password
                                </Button>
                            </Col>
                        </Form.Group> */}

                            <Form.Group as={Row} controlId="formPlaintextPhone">
                                <Form.Label column sm="1">
                                    <FaPhoneAlt style={{ width: "30px", height: "30px" }} />
                                </Form.Label>
                                <Col sm="11">
                                    <Form.Control
                                        name="mobile_phone"
                                        type="number"
                                        defaultValue={props?.data?.mobile_phone}
                                        placeholder="08xxxxxxxx"
                                        onChange={changeInput} />
                                </Col>
                            </Form.Group>
                        </Form>
                        <div
                        className="text-center py-4 mt-3 btn-div"
                        style={{justifyContent: "center", display: "flex"}}>
                            <button
                                className="save-btn"
                                variant="light"
                                type="submit"
                                style={{
                                    borderColor: "#ff415b",
                                    color: "#ff415b",
                                    width: "384px",
                                    height: "50px",
                                    fontSize: "18px",
                                    display: "flex",
                                    alignItems: "center",
                                    margin: "-0.5rem 0",
                                    justifyContent: "center"
                                }}
                                onClick={() => {submitEdit(); notify()}}
                            >
                                Save
                    </button>
                   
                        </div>
                    </Modal.Body>
                </Modal>
        </div >
    )
}

export default EditProfileModal;
