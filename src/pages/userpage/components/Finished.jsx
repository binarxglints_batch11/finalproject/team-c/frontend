import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import "./Finished.scss";
import { Card, Button } from "react-bootstrap";
import { CgSmartHomeWashMachine } from "react-icons/cg";
import { MdDone } from "react-icons/md";
import { HiLocationMarker } from "react-icons/hi";
import { IoArrowDownCircleSharp } from "react-icons/io5";
import { IcDrycl, IcIron } from "../../../assets/index.js";
import LoadingCard from "../../../common/spinner/loadingCard";
import { GetOrderFinish } from "../../../store/actions/getOrderFinish";

function Finished() {
	const dispatch = useDispatch();
	const finish = useSelector(
		(state) => state.OrderFinishDetail.userOrderFinish
	);
	const loading = useSelector((state) => state.OrderFinishDetail.loading);
	const [detail, setDetail] = useState(false);
	const [click, setClick] = useState(undefined);

	useEffect(() => {
		dispatch(GetOrderFinish());
	}, []);

	return (
		<div className="finished-container">
			{loading ? (
				<LoadingCard />
			) : finish.length ? (
				finish?.map((item, index) => {
					return (
						<Card key={index} className="card-process">
							<div className="card-title-process">
								<Card.Title>{item.laundry.name}</Card.Title>
							</div>
							<div className="order-id">
								<p>Order ID</p>
								<p className="order-id-code">#{item.id?.slice(0, 8)}</p>
							</div>

							<div className="order-notes">
								Order Finished
								<Button
									variant="outline-light"
									className="arrow-icon-process"
									onClick={() => {
										setDetail(!detail);
										click === item.id ? setClick() : setClick(item.id);
									}}
								>
									<IoArrowDownCircleSharp />
								</Button>
							</div>
							{click === item.id && (
								<div>
									<hr />
									<div className="order-item">
										<p>Service</p>
										<div className="item-type">{item.servicelist.services}</div>
									</div>
									<div className="order-item">
										<p>Weight</p>
										<div className="item-type">
											{item.weight > 0 ? `${item.weight} kilograms` : "-"}
										</div>
									</div>
									<div className="order-item-laundry">
										<p>
											{item.item?.map((x, index) => {
												return (
													<div key={index}>{`
										${x.name} x ${x.quantity}
									`}</div>
												);
											})}
										</p>
									</div>
									<hr />
									<div className="order-status">
										<p>Status : </p>
										<div className="status-order">
											{item.orderstatus.status}
										</div>
									</div>
									<div className="chart-status">
										<i
											className={
												item.orderstatus.id >= 3
													? "chart-icon-ok"
													: "chart-icon-waiting"
											}
										>
											<CgSmartHomeWashMachine />
										</i>
										<div
											className={
												item.orderstatus.id >= 4
													? "status-line-ok"
													: "status-line-waiting"
											}
										></div>
										<i
											className={
												item.orderstatus.id >= 4
													? "chart-icon-ok"
													: "chart-icon-waiting"
											}
										>
											<img
												className="icon-chart"
												src={IcDrycl}
												alt="icon-dry-cleaning"
											/>
										</i>
										<div
											className={
												item.orderstatus.id >= 5
													? "status-line-ok"
													: "status-line-waiting"
											}
										></div>
										<i
											className={
												item.orderstatus.id >= 5
													? "chart-icon-ok"
													: "chart-icon-waiting"
											}
										>
											<img
												className="icon-chart"
												src={IcIron}
												alt="icon-dry-cleaning"
											/>
										</i>
										<div
											className={
												item.orderstatus.id >= 6
													? "status-line-ok"
													: "status-line-waiting"
											}
										></div>
										<i
											className={
												item.orderstatus.id >= 6
													? "chart-icon-ok"
													: "chart-icon-waiting"
											}
										>
											<MdDone />
										</i>
									</div>
									<hr />
									<div className="ready-deliver">
										<p>Delivery</p>
										<div className="pick-up">
											<i className="loc-icon">
												<HiLocationMarker />
											</i>
											<div className="loc-item">
												<div className="pick-up-grey">Pick up at</div>
												<p>{item.pickUpAddress?.address}</p>
												<p>
													{item.pickUpAddress?.note
														? `note : ${item.pickUpAddress?.note}`
														: "-"}
												</p>
											</div>
										</div>
										<div className="deliv-line"></div>
										<div className="deliv-to">
											<i className="loc-icon">
												<HiLocationMarker />
											</i>
											<div className="loc-item">
												<div className="deliv-to-grey">Deliver to</div>
												<p>{item.deliveryAddress?.address}</p>
												<p>
													{item.deliveryAddress?.note
														? `note : ${item.deliveryAddress?.note}`
														: "-"}
												</p>
											</div>
										</div>
									</div>
								</div>
							)}
                            <hr />
							<div className="order-item">
								<p>Paid</p>
								<div className="item-type">{`Rp. ${item.totalPrice}`}</div>
							</div>
						</Card>
					);
				})
			) : (
				<Card className="finished-card">
					<div className="finished-text">
						<p>Please enjoy your time, We will process your laundry.</p>
					</div>
				</Card>
			)}
		</div>
	);
}

export default Finished;
