import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import "./ProfileCard.scss";
import { Card, Button } from "react-bootstrap";
import { ProfilePic, SettingAcc } from "../../../assets/index.js";
import { useSelector, useDispatch } from "react-redux";
import { GetUserProfile } from "../../../store/actions/GetUserProfile";
import EditProfileModal from "./EditProfile";

function ProfileCard() {
	const history = useHistory();
	const dispatch = useDispatch();
	const user = useSelector((state) => state.UserProfile.profile);
	const loading = useSelector((state) => state.UserProfile.loading);
	const idUser = localStorage.getItem('id');

	const [editProfileShow, setEditProfileShow] = useState(false);

	const toggle = () => setEditProfileShow(!editProfileShow);


	useEffect(() => {
		dispatch(GetUserProfile(idUser));
	}, []);

	const LogOut = () => {
		localStorage.clear();
		history.push("/");
	};

	// console.log("user", user);
	return (
		<div className="profile-card-container">
			<Card className="profile-card">
				<i className='ava-pic'>
					<img
						className="profile-card-pic"
						src={`https://washme.gabatch11.my.id${user?.image }`}
						alt="profile"
					/>
				</i>
				<div className="user-data">
					<div className="user-name">{user?.name}</div>
					<div>{user?.mobile_phone}</div>
					<div>{user?.email}</div>
				</div>

				<div className="user-setting" onClick={toggle}>
					<img
						className="profile-card-setting"
						src={SettingAcc}
						alt="setting"
					/>
					<div>Account Setting</div>
				</div>
				<EditProfileModal
					show={editProfileShow}
					data={user}
					onHide={toggle}
				/>
			</Card>

			<div className="btn-itemOut">
				<Button
					className="btn-logout"
					variant="outline-light"
					href="/"
					onClick={LogOut}
				>
					Log Out
				</Button>
			</div>
		</div>
	);
}

export default ProfileCard;
