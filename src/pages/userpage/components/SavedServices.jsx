import React from "react";
import "./SavedServices.scss";
import { Button, Row, Col, Card } from "react-bootstrap";
import { WashX } from "../../../assets/index.js";

function SavedServices() {
	return (
		<div className="saved-services">
			<p className="title-savedSer-container">Saved Services</p>
			<Card className="services-saved">
				<Row className="row-list-services-card">
					<Col className="col-list-services-card">
						<Card.Img variant="top" className="services-pic-img" src={WashX} />
						<div className="text-card-services">
							<div className="card-title-services">
								<Card.Title>Wash only Express</Card.Title>
								<p>2 days working</p>
							</div>
							<div className="services-price">
								<p>Rp 8000/ item</p>
							</div>
							<div className="services-price-btn">
								<p>Rp 15.000/ kilogram</p>
								<Button
									className="btn-order-svd"
									variant="light"
									// onClick={() => handleLogin()}
								>
                                    Order
                                </Button>
							</div>
						</div>
					</Col>
				</Row>
			</Card>
            <Card className="services-saved">
				<Row className="row-list-services-card">
					<Col className="col-list-services-card">
						<Card.Img variant="top" className="services-pic-img" src={WashX} />
						<div className="text-card-services">
							<div className="card-title-services">
								<Card.Title>Wash only Express</Card.Title>
								<p>2 days working</p>
							</div>
							<div className="services-price">
								<p>Rp 8000/ item</p>
							</div>
							<div className="services-price-btn">
								<p>Rp 15.000/ kilogram</p>
								<Button
									className="btn-order-svd"
									variant="light"
									// onClick={() => handleLogin()}
								>
                                    Order
                                </Button>
							</div>
						</div>
					</Col>
				</Row>
			</Card>
		</div>
	);
}

export default SavedServices;
