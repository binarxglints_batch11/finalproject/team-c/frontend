import React, { useEffect } from 'react';
import './ProcessCard.scss';
import { Nav, Tab } from 'react-bootstrap';
import ProcessItem from './ProcessItem';
import SavedLaundry from './SavedLaundry';
// import SavedServices from "./SavedServices";
import Finished from './Finished';

import { GetFavoriteLaundry } from '../../../store/actions/getFavoriteLaundry';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

function ProcessCard() {

  const dispatch = useDispatch();
  const fav = useSelector((state) => state.FavoriteLaundry.favLaundry);
  const linkTo = window.location.pathname.split("/")[0];

  useEffect(() => {
    dispatch(GetFavoriteLaundry());
  }, []);
  console.log("link", linkTo);
  return (
    <div className="process-card">
      <Tab.Container defaultActiveKey="onProcess">
        <Nav variant="pills" className="nav-tabProfile">
          <Nav.Item>
            <Nav.Link eventKey="onProcess">On process</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey="finished">Finished</Nav.Link>
          </Nav.Item>
        </Nav>
        <Tab.Content className="card-profile">
          <Tab.Pane eventKey="onProcess">
            <ProcessItem />
            {fav?.map((favo, index) => {
              return (
                <div key={index}>
                  <Link to={`${linkTo}/laundry/${favo?.laundry?.id}`}>
                    <SavedLaundry />
                  </Link>
                </div>
              )
            })}
            {/* <SavedServices/> */ }
          </Tab.Pane>
          <Tab.Pane eventKey="finished">
            <Finished />
          </Tab.Pane>
        </Tab.Content>
      </Tab.Container>
    </div>
  );
}

export default ProcessCard;
