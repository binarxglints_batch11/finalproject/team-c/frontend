import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import "./ProcessItem.scss";
import { Card, Button } from "react-bootstrap";
import { CgSmartHomeWashMachine } from "react-icons/cg";
import { MdDone } from "react-icons/md";
import { HiLocationMarker } from "react-icons/hi";
import { IoArrowDownCircleSharp } from "react-icons/io5";
import { IcDrycl, IcIron } from "../../../assets/index.js";
import { GetOrder } from "../../../store/actions/getOrder";
import LoadingCard from "../../../common/spinner/loadingCard";

function ProcessItem() {
	const dispatch = useDispatch();
	const order = useSelector((state) => state.OrderDetail.userOrder);
	const loading = useSelector((state) => state.OrderDetail.loading);
	const [detail, setDetail] = useState(false);
	const [click, setClick] = useState(undefined);

	useEffect(() => {
		dispatch(GetOrder());
	}, []);

	const orderDate = (estimateFinish) => {
		let days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
		let d = new Date(estimateFinish);

		let dayName = days[d.getDay()];
		let monthDetail = d.toString().split(" ")[1];
		let dateDay = d.toString().split(" ")[2];

		// console.log("dayName", dayName);
		// console.log("d", d);
		// console.log(typeof orderMonth);

		return (
			<span>{`This order will finish on ${dayName}, ${monthDetail} ${dateDay}`}</span>
		);
	};

	const expPayment = (expiredPayment) => {
		let days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
		let d = new Date(expiredPayment);

		let dayName = days[d.getDay()];
		let monthDetail = d.toString().split(" ")[1];
		let dateDay = d.toString().split(" ")[2];
		let hours = d.toString().split(" ")[4];

		// console.log("dayName", dayName);
		// console.log("d", d);
		// console.log()

		return (
			<span>{`Please complete your payment before ${dayName}, ${monthDetail} ${dateDay} ${hours}`}</span>
		);
	};

	return (
		<div className="card-item1">
			{loading ? (
				<LoadingCard />
			) : order.length ? (
				order?.map((item, index) => {
					return (
						<Card key={index} className="card-process">
							<div className="card-title-process">
								<Card.Title>{item.laundry.name}</Card.Title>
								{item.paymentType === "Cash" ||
								item.online_payment === "Success" ? null : (
									<a className="pay-link" href={item.redirect_url}>
										<Button className="pay-btn" variant="light">
											Please complete your payment
										</Button>
									</a>
								)}
							</div>
							<div className="order-id">
								<p>Order ID</p>
								<p className="order-id-code">#{item.id?.slice(0, 8)}</p>
								<Button
									variant="outline-light"
									className="arrow-icon-process"
									onClick={() => {
										setDetail(!detail);
										click === item.id ? setClick() : setClick(item.id);
									}}
								>
									<IoArrowDownCircleSharp />
								</Button>
							</div>
							<div className="order-notes">
								{orderDate(item.estimateFinish)}
							</div>
							{item.expiredPayment === null ||
							item.paymentType === "Cash" ? null : (
								<div className="order-notes-payment">
									{expPayment(item.expiredPayment)}
								</div>
							)}
							{click === item.id && (
								<div>
									<hr />
									<div className="order-item">
										<p>Service</p>
										<div className="item-type">{item.servicelist.services}</div>
									</div>
									<div className="order-item">
										<p>Weight</p>
										<div className="item-type">
											{item.weight > 0 ? `${item.weight} kilograms` : "-"}
										</div>
									</div>
									<div className="order-item-laundry">
										<p>
											{item.item?.map((x, index) => {
												return (
													<div key={index}>{`${x.name} x ${x.quantity}`}</div>
												);
											})}
										</p>
									</div>
									<hr />
									<div className="order-status">
										<p>Status : </p>
										<div className="status-order">
											{item.orderstatus.status}
										</div>
									</div>
									<div className="chart-status">
										<i
											className={
												item.orderstatus.id >= 3
													? "chart-icon-ok"
													: "chart-icon-waiting"
											}
										>
											<CgSmartHomeWashMachine />
										</i>
										<div
											className={
												item.orderstatus.id >= 4
													? "status-line-ok"
													: "status-line-waiting"
											}
										></div>
										<i
											className={
												item.orderstatus.id >= 4
													? "chart-icon-ok"
													: "chart-icon-waiting"
											}
										>
											<img
												className="icon-chart"
												src={IcDrycl}
												alt="icon-dry-cleaning"
											/>
										</i>
										<div
											className={
												item.orderstatus.id >= 5
													? "status-line-ok"
													: "status-line-waiting"
											}
										></div>
										<i
											className={
												item.orderstatus.id >= 5
													? "chart-icon-ok"
													: "chart-icon-waiting"
											}
										>
											<img
												className="icon-chart"
												src={IcIron}
												alt="icon-dry-cleaning"
											/>
										</i>
										<div
											className={
												item.orderstatus.id >= 6
													? "status-line-ok"
													: "status-line-waiting"
											}
										></div>
										<i
											className={
												item.orderstatus.id >= 6
													? "chart-icon-ok"
													: "chart-icon-waiting"
											}
										>
											<MdDone />
										</i>
									</div>
									<hr />
									<div className="ready-deliver">
										<p>Delivery</p>
										<div className="pick-up">
											<i className="loc-icon">
												<HiLocationMarker />
											</i>
											<div className="loc-item">
												<div className="pick-up-grey">Pick up at</div>
												<p>{item.pickUpAddress?.address}</p>
												<p>
													{item.pickUpAddress?.note
														? `note : ${item.pickUpAddress?.note}`
														: "-"}
												</p>
											</div>
										</div>
										<div className="deliv-line"></div>
										<div className="deliv-to">
											<i className="loc-icon">
												<HiLocationMarker />
											</i>
											<div className="loc-item">
												<div className="deliv-to-grey">Deliver to</div>
												<p>{item.deliveryAddress?.address}</p>
												<p>
													{item.deliveryAddress?.note
														? `note : ${item.deliveryAddress?.note}`
														: "-"}
												</p>
											</div>
										</div>
									</div>
									<hr />
									<div className="order-item">
										<p>Price</p>
										<div className="item-type">{`Rp. ${item.estimatePrice}`}</div>
									</div>
									<div className="order-item">
										<p>Admin Charge</p>
										<div className="item-type">{`Rp. ${item.admin_charge}`}</div>
									</div>
									<div className="order-item">
										<p>Delivery fee</p>
										<div className="item-type">{`Rp. ${item.delivery_fee}`}</div>
									</div>
									<hr />
									<div className="order-item">
										<p>Total</p>
										<div className="item-type">{`Rp. ${item.totalPrice}`}</div>
									</div>
								</div>
							)}
						</Card>
					);
				})
			) : (
				<Card className="card-process">
					<div className="no-process">
						<p>You don't have any orders yet</p>
					</div>
				</Card>
			)}
		</div>
	);
}

export default ProcessItem;
