import React from 'react';
import { useDispatch } from 'react-redux';
import { Container, Button, Row } from 'react-bootstrap';
import { MdHelp } from 'react-icons/md';
import './HomeComponents.scss';
import { WashIron, Wash, Iron, Dry, Shoes, Household } from '../../../assets';
import { Before } from '../../../common/before/Before';
import { getLaundries } from '../../../store/actions/getLaundries';

function HomeComponents() {
  const token = localStorage.getItem('token');

  const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(getLaundries());
  // }, []);

  const ServiceFilter = () => {
    dispatch(getLaundries());
  };

  return (
    <div className="home-container">
      {token ? (
        <>
          <Container className="first">
            <div className="homepage-text">
              <h4 className="h4home-components">What service do you need ?</h4>
              {/* <p>You can select more than one service</p> */}
            </div>

            <div className="homepage-service-button">
              <Row className="home-components">
                <Button onClick={() => ServiceFilter()} href="/services/1" id="1" className="btn-item" variant="outline-danger">
                  <img className="service-img" src={WashIron} alt="wash-iron" />
                  <p className="phome-components">Wash & Iron</p>
                </Button>
                <Button onClick={() => ServiceFilter()} href="/services/2" id="2" className="btn-item" variant="outline-danger">
                  <img className="service-img" src={Wash} alt="wash-iron" />
                  <p className="phome-components">Wash</p>
                </Button>
                <Button
                  onClick={() => ServiceFilter()}
                  href="/services/3"
                  id="3"
                  // id={id}
                  // name={service}
                  className="btn-item"
                  variant="outline-danger"
                >
                  <img className="service-img" src={Iron} alt="wash-iron" />
                  <p className="phome-components">Iron</p>
                </Button>
                <Button onClick={() => ServiceFilter()} href="/services/4" id="4" className="btn-item" variant="outline-danger">
                  <img className="service-img" src={Dry} alt="wash-iron" />
                  <p className="phome-components">Dry Clean</p>
                </Button>
                <Button onClick={() => ServiceFilter()} href="/services/5" id="5" className="btn-item" variant="outline-danger">
                  <img className="service-img" src={Shoes} alt="wash-iron" />
                  <p className="phome-components">Shoes</p>
                </Button>
                <Button onClick={() => ServiceFilter()} href="/services/6" id="6" className="btn-item" variant="outline-danger">
                  <img className="service-img" src={Household} alt="wash-iron" />
                  <p className="phome-components">Household</p>
                </Button>
              </Row>
            </div>
          </Container>
          {/* <Container className="second">
        <div className="homepage-text">
          <h5 className="h5home-components">Where are you ?</h5>
        </div>
        <Form role="search" className="search-laundry">
          <i className="loc">
            <HiLocationMarker />
          </i>
          <FormControl className="search-input" type="text" placeholder="Type your address" />
        </Form>
        <Button
          href='/services'
          className="btn-find"
          style={{
            marginTop: '2rem',
            width: '15rem',
            backgroundColor: '#FF6C6C',
            borderColor: '#FF6C6C',
            color: '#FFFFFF',
          }}
          variant="light"
        >
          Find your ideal laundry now
        </Button>
      </Container> */}
          <br />
          <br />
          <div className="btn-help">
            <Button className="help-button" variant="light">
              <MdHelp />
              Need Help
            </Button>
            <br />
            <br />
            <br />
          </div>
        </>
      ) : (
        // "Please login to access this page :)"
        <Before />
      )}
    </div>
  );
}

export default HomeComponents;
