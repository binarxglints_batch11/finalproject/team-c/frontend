import React from 'react';
import './Homepage.scss';
import NavbarComponent from '../../common/navbar/Navbar';
import HomeComponents from './components/HomeComponents';
import Footer from '../../common/footer/Footer';

function Homepage() {
  return (
    <div className="homepage">
      <NavbarComponent />
      <HomeComponents />
      <Footer />
    </div>
  );
}

export default Homepage;
