import React from 'react';
import './AboutUsPage.scss';
import AboutUsComponent from './component/AboutUs';
import NavbarComponent from '../../common/navbar/Navbar';
import AdvBanner from '../../common/banner/AdvBanner';
import Footer from '../../common/footer/Footer';

function AboutUsPage() {
  return (
    <div className="about-us">
      <NavbarComponent />
      <AboutUsComponent />
      <AdvBanner />
      <Footer />
    </div>
  );
}

export default AboutUsPage;