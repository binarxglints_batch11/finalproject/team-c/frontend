import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './AboutUs.scss';
import { RectangleBG, Weight, Time, Shield, EllipseBG, AboutIMG } from '../../../assets/index.js';
import { Divider } from '@material-ui/core';

const AboutUs = () => {
  return (
    <>
      <div className="header-aboutus">
        <h5 className="header-h5">
          Wash Me merupakan platform Kemitraan Usaha Laundry (partnership) yang didukung oleh teknologi online yang akan memberikan kemudahan baik kepada seluruh stakeholder baik pelanggan, partner maupun investor (Passive Partner)
        </h5>
      </div>

      <Container className="main-container-aboutus">
        <div className="why-aboutus">
          <div className="col-1">
            <h3 className="title-h3">Why Wash Me?</h3>
          </div>

          <Row className="roww">
            <div className="col-2">
              <div className="div-1">
                <img src={Weight} alt="weight" />
                <h5 className="title-h5">Now all are clean!</h5>
              </div>
              <div className="div-2">
                <p className="title-p">See laundries around you with variety of services. Once you find one that fit you, set your order!</p>
              </div>
            </div>

            <div className="col-3">
              <div className="div-1">
                <img src={Time} alt="time" />
                <h5 className="title-h5">Now all are clean!</h5>
              </div>
              <div className="div-2">
                <p className="title-p">See laundries around you with variety of services. Once you find one that fit you, set your order!</p>
              </div>
            </div>
          </Row>

          <div className="col-4">
            <div className="div-1">
              <img src={Shield} alt="shield" />
              <h5 className="title-h5">Now all are clean!</h5>
            </div>
            <div className="div-2">
              <p className="title-p">See laundries around you with variety of services. Once you find one that fit you, set your order!</p>
            </div>
          </div>
        </div>
      </Container>

      <div className="vision-mission-aboutus">
        <div className="roww">
          <div className="img-1">
            <img src={AboutIMG} alt="abouttt" />
          </div>

          <div className="col-1">
            <div className="title-div">
              <h3 className="title-h3">Our vision & mission</h3>
            </div>

            <div className="vision-mission-div">
              <div className="div-1">
                <h5 className="title-h5">Vision</h5>
                <p className="title-p">See laundries around you with variety of services. Once you find one that fit you, set your order! See laundries around you with variety of services. Once you find one that fit you, set your order!</p>
              </div>
              <div className="div-1">
                <h5 className="title-h5">Mission</h5>
                <p className="title-p">See laundries around you with variety of services. Once you find one that fit you, set your order! See laundries around you with variety of services. Once you find one that fit you, set your order!</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AboutUs;
