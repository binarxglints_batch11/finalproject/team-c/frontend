import React, { useState } from 'react';
import './Search.scss';
import FilterBar from '../../common/filterbar/FilterBar';
import Footer from '../../common/footer/Footer';
import NavbarComponent from '../../common/navbar/Navbar';
import LaundryList from './component/SearchDetail';
import { Before } from '../../common/before/Before';

const SearchPage = () => {
  const token = localStorage.getItem('token');
  const [all, setAll] = useState(false);

  return (
    <div className="servicepage">
      <NavbarComponent setAll={setAll} />
      {token ? (
        <>
          <FilterBar />
          <LaundryList all={all} setAll={setAll} />
        </>
      ) : (
        <Before />
      )}
      <Footer />
    </div>
  );
};

export default SearchPage;
