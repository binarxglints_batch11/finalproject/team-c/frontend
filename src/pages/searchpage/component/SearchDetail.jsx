import { React, useState, useRef, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import './SearchDetail.scss';
import { Card, Container, Row, Col } from 'react-bootstrap';
// import { WashMach, DelivBike, WashIron1, WashIronX } from '../../../assets/index.js';
import { MdStar } from 'react-icons/md';
import Pagination from '@material-ui/lab/Pagination';
import { useSelector, useDispatch } from 'react-redux';
import { GetFilterServices } from '../../../store/actions/GetServices';
import { GetAllLaundry } from '../../../store/actions/GetAllLaundry';
import { GetSearchName } from '../../../store/actions/GetSearchName';
// import { GetSearchFilter } from '../../../store/actions/GetSearchFilter';

const LaundryList = ({ ...props }) => {
  const [show, setShow] = useState(true);
  const target = useRef(null);
  let { id } = useParams();

	const dispatch = useDispatch();
	const service = useSelector((state) => state.FilterServices.services);
	const loading = useSelector((state) => state.FilterServices.loading);
	const allServices = useSelector((state) => state.AllLaundry.allLaundry);
	const laundryName = useSelector((state) => state.SearchName.searchByName);
	// console.log(laundryName)

  useEffect(() => {
    dispatch(GetFilterServices(id));
    if (id === 'all') {
      dispatch(GetAllLaundry());
    }
    dispatch(GetSearchName(id));
  }, []);

  // const filter = useSelector((state) => state.SearchFilter.searchLaundry);
  // const load = useSelector((state) => state.SearchFilter.loading);

  return (
    <div>
      <div className="pointer" ref={target} onClick={() => setShow(!show)}>
        <Container className="laundrylist-container">
          {
            loading
              ? 'loading'
              : allServices?.data?.length
              ? allServices?.data?.map((item, index) => {
                  return (
                    <Card key={index} className="laundry-wrapper">
                      <Row>
                        <Col className="laundry-img">
                          {/* {console.log(
													`https://washme.gabatch11.my.id${item.image}`
												)} */}
                          <Card.Img src={`https://washme.gabatch11.my.id${item.image}`} alt="Laundry-pic" />
                        </Col>
                        <Col className="laundry-content">
                          <Card.Title className="laundry-title"> {item.name}</Card.Title>
                          <Card.Subtitle className="laundry-address">{item.address.street}</Card.Subtitle>
                          <Card.Text className="laundry-services">{item.total_services} services</Card.Text>
                          <Card.Text className="laundry-order">
                            <img className="laundry-icon" src="https://i.ibb.co/vDQsMgT/delivery-bike.png" alt="Google login Logo" />
                            Pick up & Delivery
                          </Card.Text>
                        </Col>
                        <Col className="laundry-content">
                          <Card.Title className="laundry-review">
                            <MdStar className="laundry-rating" /> {item.average_rating}
                          </Card.Title>
                          <Card.Text className="laundry-type">By Kilogram</Card.Text>
                          <Card.Text className="laundry-type">By Item</Card.Text>
                        </Col>
                      </Row>
                    </Card>
                  );
                })
              : service?.data?.map((item, index) => {
                  return (
                    <Card key={index} className="laundry-wrapper">
                      <Row>
                        <Col className="laundry-img">
                          <Card.Img src={`https://washme.gabatch11.my.id${item.image}`} alt="Laundry-pic" />
                        </Col>
                        <Col className="laundry-content">
                          <Card.Title className="laundry-title">{item.name}</Card.Title>
                          <Card.Subtitle className="laundry-address">{item.address.street}</Card.Subtitle>
                          <Card.Text className="laundry-services">{item.total_services} services</Card.Text>
                          <Card.Text className="laundry-order">
                            <img className="laundry-icon" src="https://i.ibb.co/vDQsMgT/delivery-bike.png" alt="Google login Logo" />
                            Pick up & Delivery
                          </Card.Text>
                        </Col>
                        <Col className="laundry-content">
                          <Card.Title className="laundry-review">
                            <MdStar className="laundry-rating" /> {item.average_rating}
                          </Card.Title>
                          <Card.Text className="laundry-type">By Kilogram</Card.Text>
                          <Card.Text className="laundry-type">By Item</Card.Text>
                        </Col>
                      </Row>
                    </Card>
                  );
                })
            // 	? laundryName?.data?.map((item, index) => {
            // 			return (
            // 				<Card key={index} className="laundry-wrapper">
            // 					<Row>
            // 						<Col className="laundry-img">
            // 							<Card.Img
            // 								src={`https://washme.gabatch11.my.id${item.image}`}
            // 								alt="Laundry-pic"
            // 							/>
            // 						</Col>
            // 						<Col className="laundry-content">
            // 							<Card.Title className="laundry-title">
            // 								{" "}
            // 								{item.name}
            // 							</Card.Title>
            // 							<Card.Subtitle className="laundry-address"></Card.Subtitle>
            // 							<Card.Text className="laundry-services">
            // 								{item.total_services} services
            // 							</Card.Text>
            // 							<Card.Text className="laundry-order">
            // 								<img
            // 									className="laundry-icon"
            // 									src="https://i.ibb.co/vDQsMgT/delivery-bike.png"
            // 									alt="Google login Logo"
            // 								/>
            // 								Pick up & Delivery
            // 							</Card.Text>
            // 						</Col>
            // 						<Col className="laundry-content">
            // 							<Card.Title className="laundry-review">
            // 								<MdStar className="laundry-rating" />{" "}
            // 								{item.average_rating}
            // 							</Card.Title>
            // 							<Card.Text className="laundry-type">
            // 								By Kilogram
            // 							</Card.Text>
            // 							<Card.Text className="laundry-type">
            // 								By Item
            // 							</Card.Text>
            // 						</Col>
            // 					</Row>
            // 				</Card>
            // 			);
            // 	  })
            // : "No laundry found"
          }
        </Container>
      </div>

      <div className="pagination">
        <Pagination count={5} />
      </div>
    </div>
  );
};

export default LaundryList;
