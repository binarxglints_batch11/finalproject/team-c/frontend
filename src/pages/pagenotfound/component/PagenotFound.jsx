import React from 'react';
import './Error.css';
import { Errorimg } from '../../../assets';

const PagenotFound = () => {
  return (
    <div>
      <img className="error" src={Errorimg} alt="error loh..." />
    </div>
  );
};

export default PagenotFound;
