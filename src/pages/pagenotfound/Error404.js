import React from 'react';
import NavbarComponent from '../../common/navbar/Navbar';
import PagenotFound from './component/PagenotFound';

const Error404 = () => {
  return (
    <div>
      <NavbarComponent />
      <PagenotFound />
    </div>
  );
};

export default Error404;
