import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import './OrderpageNew.scss';
import { useLocation } from 'react-router-dom';
// import { AiOutlineArrowRight } from "react-icons/ai";
import { Container } from "react-bootstrap";
import NavbarComponent from "../../common/navbar/Navbar";
import SetOrder from "./components/SetOrder";
import SetAddress from "./components/SetAddress";
import Payment from "./components/Payment";
import Ready from "./components/Ready";
import LaundryCard from "./components/LaundryCard";
// import ListBtn from "../../assets/list-btn.png";

const Orderpage = (props) => {
	let location = useLocation();
	const laundry = useSelector((state) => state.OrderItem);
	const [stage, setStage] = useState(0);
	const [detailOrder, setDetailOrder] = useState({
		laundry_id: "",
		ordertype_id: "",
		servicelist_id: "",
		servicelevel_id: "",
		weight: 0,
		item: [],
		estimatePrice: "",
		pickUpChoice: "Delivery",
		pickUpAddress: {},
		deliveryAddress: {},
		delivery_fee: "12000",
		admin_charge: "7000",
		totalPrice: "",
		paymentType: "",
	});

	useEffect(() => {
		setDetailOrder({
			...detailOrder,
			laundry_id: laundry?.laundry,
			servicelist_id: laundry?.servicelist?.toString(),
			servicelevel_id: laundry?.servicelevel?.toString(),
		});
	}, [laundry]);

  const sendOrder = (name, value) => {
    setDetailOrder({
      ...detailOrder,
      [name]: value,
    });
  };

//   console.log('detailOrder', detailOrder)

	return (
		<div className="order-page">
			<NavbarComponent />
			<Container className="order-container">
				{stage === 0 && (
					<div className="set-order">
						<div className="title-tab">
							<p className="active">Set your order</p>
							<p>Set Address</p>
							<p>Select Payment</p>
							<p>Your order is ready!</p>
						</div>
						<div className="set-card">
							<LaundryCard data={location.state} sendOrder={sendOrder} />
							<SetOrder
								data={location.state}
								sendOrder={sendOrder}
								detailOrder={detailOrder}
								next={setStage}
							/>
						</div>
					</div>
				)}
				{stage === 1 && (
					<div className="set-address">
						<div className="title-tab">
							<p>Set your order</p>
							<p className="active">Set Address</p>
							<p>Select Payment</p>
							<p>Your order is ready!</p>
						</div>
						<div className="set-card">
							<LaundryCard data={location.state} sendOrder={sendOrder} />
							<SetAddress
								sendOrder={sendOrder}
								next={setStage}
								detailOrder={detailOrder}
							/>
						</div>
					</div>
				)}
				{stage === 2 && (
					<div className="set-payment">
						<div className="title-tab">
							<p>Set your order</p>
							<p>Set Address</p>
							<p className="active">Select Payment</p>
							<p>Your order is ready!</p>
						</div>
						<div className="set-card">
							<LaundryCard data={location.state} />
							<Payment
								sendOrder={sendOrder}
								next={setStage}
								detailOrder={detailOrder}
							/>
						</div>
					</div>
				)}
				{stage === 3 && (
					<div className="set-summary">
						<div className="title-tab">
							<p>Set your order</p>
							<p>Set Address</p>
							<p>Select Payment</p>
							<p className="active">Your order is ready!</p>
						</div>
						<div className="set-card">
							<LaundryCard data={location.state} />
							<Ready sendOrder={sendOrder} detailOrder={detailOrder} setDetailOrder={setDetailOrder}/>
						</div>
					</div>
				)}
			</Container>
			{/* <Container className="order-container">
				<Tab.Container defaultActiveKey="setOrder">
					<Nav variant="pills" className="nav-tab">
						<Nav.Item>
							<Nav.Link eventKey="setOrder">Set your order</Nav.Link>
						</Nav.Item>
						<Nav.Item>
							<Nav.Link eventKey="setAddress">Set Address</Nav.Link>
						</Nav.Item>
						<Nav.Item>
							<Nav.Link eventKey="payment">Select Payment</Nav.Link>
						</Nav.Item>
						<Nav.Item>
							<Nav.Link eventKey="ready">Your order is ready!</Nav.Link>
						</Nav.Item>
					</Nav>
					<Tab.Content className="card-orderpage">
						<Tab.Pane eventKey="setOrder">
							<div className="set-order">
								<LaundryCard data={location.state} />
								<SetOrder data={location.state} order={detailOrder} />
							</div>
						</Tab.Pane>
						<Tab.Pane eventKey="setAddress">
							<div className="set-order">
								<LaundryCard data={location.state} />
								<SetAddress setAddress={address} />
							</div>
						</Tab.Pane>
						<Tab.Pane eventKey="payment">
							<div className="set-order">
								<LaundryCard data={location.state} />
								<Payment />
							</div>
						</Tab.Pane>
						<Tab.Pane eventKey="ready">
							<div className="set-order">
								<LaundryCard data={location.state} />
								<Ready />
							</div>
						</Tab.Pane>
					</Tab.Content>
					<div className="btn-setorder">
						<Button
							className="btn-setorder-list"
							variant="outline-light"
							type="submit"
						>
							Add To List
							<div className="list-btn-icon">
								<img src={ListBtn} alt="list icon btn" />
							</div>
						</Button>
						<Button
							className="btn-setorder-next"
							variant="light"
							// disabled={item && clothesItem.length && totalClothes ? false : true}
							// onClick={() => order()}
						>
							Next
							<div className="arrow-icon">
								<AiOutlineArrowRight />
							</div>
						</Button>
					</div>
				</Tab.Container>
			</Container> */}
    </div>
  );
};

export default Orderpage;
