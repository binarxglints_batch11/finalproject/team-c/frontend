import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { AiFillStar } from 'react-icons/ai';
import './LaundryCard.scss';
import { Card } from 'react-bootstrap';
import { DelivBike } from '../../../assets/index.js';
import { GetDataLaundry } from '../../../store/actions/getDataLaundry';
import { SetOrder } from '../../../store/actions/setOrder';

const LaundryCard = ({...props}) => {
	const laundry = useSelector((state) => state.DataLaundry.dataLaundry);
	const dispatch = useDispatch();
	const IdPath = window.location.pathname.split("/")[2];
	const itemOrder = {
		laundry: props.data.id,
		servicelevel: props.data.service.id,
		servicelist: props.data.serviceId
	}
	

  useEffect(() => {
    dispatch(SetOrder(itemOrder));
  }, []);

  return (
    <>
      <div className="laundry-container">
        <Card className="detail-card">
          <div className="card-no-margin">
            <div className="list-detail-card">
              <Card.Img variant="top" className="laundry-pic-img" src={`https://washme.gabatch11.my.id/${laundry?.image}`} />
              <div className="list-detail-card-noflex">
                <div className="list-detail-card-flex">
                  <div className="text-card">
                    <div className="card-title-name">
                      <Card.Title>{laundry?.name}</Card.Title>
                    </div>
                    <div className="laundry-loc">
                      <p>{laundry?.address?.street}</p>
                      {/* <p className="loc-distance">0.7 km</p> */}
                    </div>
                    <div className="laundry-services">
                      <p>{laundry?.total_services} services</p>
                    </div>
                  </div>
                  <div className="text-card-rating">
                    <div className="rating-text">
                      <AiFillStar className="rating-star-item" />
                      <p>
                        {laundry?.average_rating} ({laundry?.total_review} reviews)
                      </p>
                    </div>
                    <div className="office-hours-text">
                      <p>Open 09.00 am - 09.00 pm</p>
                    </div>
                  </div>
                </div>
                <div className="laundry-delivery">
                  <p>{laundry?.pickUpAndDelivery ? 'Pick up & Delivery' : null}</p>
                  <img className="pic-bike" src={DelivBike} alt="delivery logo" />
                </div>
              </div>
            </div>
            <hr />
            <div className="list-service-card">
              <i className="icon-order">
                <img variant="top" className="service-icon" src={`https://washme.gabatch11.my.id/${props.data.service.image}`} alt="icon" />
              </i>
              <div className="detail-service">
                <div className="text-detail">
                  <div className="card-title-detail">
                    <Card.Title>{`${props.data.name} ${props.data.service.name}`}</Card.Title>
                    <p>{`${props.data.service.days}  days working`}</p>
                  </div>
                  {/* <div className="price">
										<p>Rp {props.data.itemPrice}/item</p>
									</div> */}
                  <div className="price">
                    <p>Detail price by item</p>
                    {props.data.service.byItem.item?.map((detail, index) => {
                      return (
                        <p key={index}>
                          {detail.name} : Rp {detail.price}/item
                        </p>
                      );
                    })}
                  </div>
                  <br />
                  {props.data.service.byWeight ? (
                    <div className="price">
                      <p>Detail price by kilogram</p>
                      <p>Rp {props.data.service.byWeight.price}/kilogram</p>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </Card>
      </div>
    </>
  );
};

export default LaundryCard;
