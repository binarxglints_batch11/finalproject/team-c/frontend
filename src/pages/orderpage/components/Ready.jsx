import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import './Ready.scss';
import { HiLocationMarker } from 'react-icons/hi';
import { Button, Card, Form } from 'react-bootstrap';
import { PostOrder } from '../../../store/actions/postOrder';

function Ready({ ...props }) {
  const dispatch = useDispatch();
  const history = useHistory();
  const id = localStorage.getItem('id');
  const [ok, setOk] = useState(false);

  const handleNext = () => setOk(!ok);

  const itemObj = props.detailOrder.item;
  const weightNum = props.detailOrder.weight;
  const type = Number(props.detailOrder.ordertype_id);
  // console.log('type', typeof type)

  useEffect(() => {
    props.setDetailOrder({
      ...props.detailOrder,
      item: JSON.stringify(itemObj),
      weight: weightNum.toString(),
    });
  }, []);

  const Order = () => {
    dispatch(PostOrder(props.detailOrder));
    history.push(`/profile/${id}`);
  };

  const addressPickUp = JSON.parse(props.detailOrder.pickUpAddress);

  const addressDelivery = JSON.parse(props.detailOrder.deliveryAddress);

  return (
    <div className="ready-container">
      <Card className="ready-card">
        <div className="ready-deliver">
          <p>Delivery Address</p>
          <div className="pick-up">
            <i className="loc-icon">
              <HiLocationMarker />
            </i>
            <div className="loc-item">
              <div className="pick-up-grey">Pick up at</div>
              <p>{addressPickUp.address}</p>
            </div>
          </div>
          <div className="deliv-line"></div>
          <div className="deliv-to">
            <i className="loc-icon">
              <HiLocationMarker />
            </i>
            <div className="loc-item">
              <div className="deliv-to-grey">Deliver to</div>
              <p>{addressDelivery.address}</p>
            </div>
          </div>
        </div>
        <div className="type-summary">
          <div className="type-only">
            <p>Type</p>
            <div className="item">{type === 1 ? 'By Item' : 'By Kilogram'}</div>
          </div>
          {props.detailOrder.weight ? (
            <div className="type-item">
              <p>Weight</p>
              {props.detailOrder.weight > 0 ? <div>{props.detailOrder.weight} Kilograms</div> : '-'}
            </div>
          ) : null}

          <div className="type-item">
            <p>Payment</p>
            <div>{props.detailOrder.paymentType}</div>
          </div>
        </div>
        <hr />
        <div className="price-summary">
          <div className="price-item">
            <p>Subtotal</p>
            <div>{props.detailOrder.estimatePrice}</div>
          </div>
          <div className="price-item">
            <p>Delivery fee</p>
            <div>{props.detailOrder.delivery_fee}</div>
          </div>
          <div className="price-item">
            <p>Admin Charge</p>
            <div>{props.detailOrder.admin_charge}</div>
          </div>
        </div>
        <hr />
        <div className="total-summary">
          <div className="total-item">
            <p>Total payment</p>
            <div>{props.detailOrder.totalPrice}</div>
          </div>
        </div>
        <div className="notes">The price may change if the weight of the clothes inputted does not match when it is calculated by the courier during pick up.</div>
        <Form.Check type="checkbox" label="I Understand" onClick={handleNext} />
      </Card>
      {ok ? (
        <div className="btn-order">
          <Button className="btn-order-now" variant="light" onClick={Order}>
            Order Now
          </Button>
        </div>
      ) : null}
    </div>
  );
}

export default Ready;
