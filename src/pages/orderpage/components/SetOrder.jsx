import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
// import { useDispatch } from "react-redux";
import "./SetOrder.scss";
import { AiOutlineArrowRight } from "react-icons/ai";
import { FaTimes } from "react-icons/fa";
import { MdDeleteForever } from "react-icons/md";
import Select from "react-select";
import { Button, Card, Form, Row, Col } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ListBtn from "../../../assets/list-btn.png";

function SetOrder({ ...props }) {
	let location = useLocation();
	const [selectValue, setSelectValue] = useState(false);
	const [countKG, setCountKG] = useState(0);
	const [countItem, setCountItem] = useState(0);
	const [addClothes, setAddClothes] = useState("");
	const [idClothes, setIdClothes] = useState("");
	const [item, setItem] = useState([]);
	const [type, setType] = useState("");
	const [ok, setOk] = useState(false);

	console.log("props-order", props.data);
	// console.log("props-setorder", location);

	const handleNext = () => setOk(!ok);

	const orderItem =
		props.data.service.byItem && props.data.service.byWeight
			? [
					{ value: 1, label: "By Item" },
					{ value: 2, label: "By Kilogram" },
			  ]
			: [{ value: 1, label: "By Item" }];


	// console.log("orderItem", orderItem);

	const clothes = props.data.service.byItem.item.map((item, index) => {
		return {
			value: item.id,
			label: item.name,
		};
	});
	// console.log('clothes', clothes)

	const handleChange = (e) => {
		if (e.value === 2) {
			setSelectValue(true);
		} else {
			setSelectValue(false);
		}

		props.sendOrder("ordertype_id", e.value.toString());

		setType({ value: e.value });
		// console.log("setType", setType);
	};
	// console.log("type", type);

	const typeStr = Number(props.detailOrder.ordertype_id);

	useEffect(() => {
		props.sendOrder("weight", countKG);
	}, [countKG]);
	// console.log("weight", countKG);
	// console.log("item", countItem);

	const handleChangeClothes = (e) => {
		setAddClothes(e.label);
		setIdClothes(e.value);
	};
	// console.log("addClothes", addClothes);
	// console.log("idClothes", idClothes);

	// nama variable di sesuikan sama data BE
	const handleAddItem = () => {
		const newItem = [
			...item,
			{ name: addClothes, quantity: countItem, id: idClothes },
		];

		setItem(newItem);
		setTimeout(function () {
			setCountItem(0);
		}, 500);
		props.sendOrder("item", newItem);

		console.log("newItem", typeof newItem);
	};

	const itemStr = props.detailOrder.item;

	// console.log("item", typeof itemStr);

	// const dataItem = props.data.service.byItem.item;
	const dataDetailOrder = props.detailOrder.item;
	const price = dataDetailOrder?.map((item) => {
		const dataItem = props.data.service.byItem.item.find(
			(service) => service.id === item.id
		);
		return {
			...item,
			total: dataItem.price * item.quantity,
		};
	});
	const sumTotal = price.reduce((acc, data) => {
		return acc + data.total;
	}, 0);

	// console.log("price", price);
	// console.log("sumTotal", sumTotal);

	const sumItem = () => {
		props.sendOrder("estimatePrice", sumTotal.toString());
	};

	// const sumItem = () => {
	// 	const total = props.detailOrder.item?.reduce((acc, data) => {
	// 		const selected = location.state.service.byItem.item?.find(
	// 			(service) => service.id === data.id
	// 		);

	// 		const estimate = data.quantity * selected.price;
	// 		return acc + estimate;
	// 	}, 0);
	// 	props.sendOrder("estimatePrice", total.toString());
	// };

	const sumWeight = () => {
		const sum = countKG * location.state.service.byWeight.price;
		props.sendOrder("estimatePrice", sum.toString());
	};

	const deleteItem = (name) => {
		setItem(item.filter((item) => item.name !== name));
	};

	const incrementKG = (e) => {
		setCountKG(countKG + 1);
	};

	const decrementKG = () => {
		if (countKG === 0) {
			return;
		}
		setCountKG(countKG - 1);
	};

	const incrementItem = () => {
		setCountItem(countItem + 1);
	};

	const decrementItem = () => {
		if (countItem === 0) {
			return;
		}
		setCountItem(countItem - 1);
	};

	const notifyAddWeight = () =>
		toast("🦄 please input your weight and item quantity", {
			position: "top-right",
			autoClose: 5000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
		});

	const notifyAddItem = () =>
		toast("🦄 please input your item quantity", {
			position: "top-right",
			autoClose: 5000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
		});

	const notifyNull = () =>
		toast("🦄 please check your order quantity", {
			position: "top-right",
			autoClose: 5000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
		});

	const okNext = () => {
		if (type.value === 2 && countKG === 0 && item.length === 0) {
			notifyAddWeight();
		} else if (type.value === 1 && item.length === 0) {
			notifyAddItem();
		} else if (type.value === 2 && item.length === 0) {
			notifyAddItem();
		} else if (type === "") {
			notifyNull();
		} else {
			props.next(1);
		}
	};

	return (
		<div className="setorder-container">
			<Card className="setorder-card">
				<div className="setorder-card-padding">
					<div className="card-title-order">
						<Card.Title>Type</Card.Title>
					</div>
					<Select
						className="type-select"
						options={orderItem}
						selected={selectValue}
						onChange={handleChange}
						name="type"
					/>
					{selectValue ? (
						<div className="add-weight">
							<div className="card-title-order-bottom">
								<Card.Title>Weight</Card.Title>
							</div>
							<p>{props.data.service.byWeight.price}/kilogram</p>
							<div className="counter">
								<Button
									variant="light"
									className="count-kg-btnd"
									onClick={decrementKG}
								>
									-
								</Button>
								<div className="count-kg">
									<div>{countKG}</div>
									<p>kg</p>
								</div>
								<Button
									variant="light"
									className="count-kg-btni"
									onClick={incrementKG}
								>
									+
								</Button>
							</div>
						</div>
					) : null}

					<div className="card-title-order-bottom">
						<Card.Title>Clothes Detail</Card.Title>
					</div>
					<Row>
						<Col>
							<Select
								className="clothes-select"
								options={clothes}
								selected={selectValue}
								onChange={handleChangeClothes}
							/>
						</Col>
						<Col>
							<div className="counter">
								<Button
									variant="light"
									className="count-item-btnd"
									onClick={decrementItem}
								>
									-
								</Button>
								<div className="count-item">
									<div name="weight">{countItem}</div>
								</div>
								<Button
									variant="light"
									className="count-item-btni"
									onClick={incrementItem}
								>
									+
								</Button>
							</div>
						</Col>
					</Row>

					<Button
						className="add-button"
						variant="light"
						onClick={countItem ? handleAddItem : null}
						name="item"
					>
						+ Add
					</Button>

					<div className="item-list">
						{item && item.length
							? item.map((clothes, index) => {
									return (
										<div className="item-selected" key={index}>
											<p className="item-name">{clothes.name}</p>
											<FaTimes />
											<p>{clothes.quantity}</p>
											<p>pcs</p>
											<Button
												className="delete-item-button"
												variant="outline-light"
												onClick={() => deleteItem(clothes.name)}
											>
												<MdDeleteForever />
											</Button>
										</div>
									);
							  })
							: null}
					</div>
					<div className="notes">
						The price may change if the weight of the clothes inputted does not
						match when it is calculated by the courier during pick up.
					</div>
					<Form.Check
						type="checkbox"
						label="I Understand"
						onClick={handleNext}
					/>
				</div>
			</Card>
			<ToastContainer
				position="top-right"
				autoClose={5000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
			/>
			{ok ? (
				<div className="btn-setorder">
					{/* <Button
						className="btn-setorder-list"
						variant="outline-light"
						type="submit"
					>
						Add To List
						<div className="list-btn-icon">
							<img src={ListBtn} alt="list icon btn" />
						</div>
					</Button> */}
					<Button
						className="btn-setorder-next"
						variant="light"
						onClick={() => {
							okNext();
							if (typeStr === 1) {
								sumItem();
							} else {
								sumWeight();
							}
						}}
					>
						Next
						<div className="arrow-icon">
							<AiOutlineArrowRight />
						</div>
					</Button>
				</div>
			) : null}
		</div>
	);
}

export default SetOrder;
