import React, { useState } from "react";
import "./Payment.scss";
import { AiOutlineArrowRight, AiFillCheckCircle } from "react-icons/ai";

import { Button, Card } from "react-bootstrap";

function Payment({ ...props }) {
	const [paymentType, setPaymentType] = useState("");

	const handlePayment = (e) => {
		setPaymentType(e.target.innerText);
		props.sendOrder("paymentType", e.target.innerText);
	};

	return (
		<div className="pay-container">
			<Card className="pay-card">
				<div className="card-title-pay">
					<Card.Title>Please select your payment</Card.Title>
				</div>

				<Button
					className="pay-cash-button"
					variant="primary"
					onClick={handlePayment}
				>
					<p>Cash</p>
					{paymentType === "Cash" ? (
						<div className="check-icon">
							<AiFillCheckCircle />
						</div>
					) : null}
				</Button>
				<Button
					className="pay-e-button"
					variant="primary"
					onClick={handlePayment}
				>
					<p>Debit/credit card</p>
					{paymentType === "Debit/credit card" ? (
						<div className="check-icon">
							<AiFillCheckCircle />
						</div>
					) : null}
				</Button>
				<Button
					className="pay-e-button"
					variant="primary"
					onClick={handlePayment}
				>
					<p>E-wallet</p>
					{paymentType === "E-wallet" ? (
						<div className="check-icon">
							<AiFillCheckCircle />
						</div>
					) : null}
				</Button>
			</Card>
			{paymentType === "" ? null : (
				<div className="btn-pay">
					<Button
						className="btn-pay-next"
						variant="light"
						onClick={() => props.next(3)}
					>
						Next
						<div className="arrow-icon">
							<AiOutlineArrowRight />
						</div>
					</Button>
				</div>
			)}
		</div>
	);
}

export default Payment;
