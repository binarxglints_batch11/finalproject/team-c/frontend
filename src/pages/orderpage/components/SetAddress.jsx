import React, { useState, useEffect } from "react";
import "./SetAddress.scss";
import { HiLocationMarker } from "react-icons/hi";
import { AiOutlineArrowRight } from "react-icons/ai";
import { FaPen } from "react-icons/fa";
import { ToastContainer, toast } from "react-toastify";
import { FormControl, Button, Card, Form } from "react-bootstrap";

const SetAddress = ({ ...props }) => {
	const [notePickUp, setNotePickUp] = useState(false);
	const [noteDeliv, setNoteDeliv] = useState(false);
	const [pickup, setPickUp] = useState("");
	const [pickupNote, setPickUpNote] = useState("");
	const [deliver, setDeliver] = useState("");
	const [deliverNote, setDeliverNote] = useState("");
	const [addressPickUp, setAddressPickUp] = useState({});
	const [addressDeliver, setAddressDeliver] = useState({});
	const [sameAddress, setSameAddress] = useState(false);
	const delivery_fee = 12000;
	const admin_charge = 7000;

	const estimate = parseInt(props.detailOrder.estimatePrice);

	const handleCheck = () => setSameAddress(!sameAddress);

	const handlePickUp = (e) => {
		setPickUp(e.target.value);
		const justPickUp = {
			...addressPickUp,
			address: pickup,
			note: '',
		};
		setAddressPickUp(justPickUp);
		props.sendOrder("pickUpAddress", JSON.stringify(justPickUp));
	};

	const btnNotePickUp = () => setNotePickUp(!notePickUp);

	const handlePickUpComplete = (e) => {
		setPickUpNote(e.target.value);
		const completeAddress = {
			...addressPickUp,
			address: pickup,
			note: pickupNote,
		};

		setAddressPickUp(completeAddress);
		props.sendOrder("pickUpAddress", JSON.stringify(completeAddress));
	};

	const handleDeliv = (e) => {
		setDeliver(e.target.value);
		const justDeliver = {
			...addressDeliver,
			address: deliver,
			note: '',
		};
		setAddressDeliver(justDeliver);
		props.sendOrder("deliveryAddress", JSON.stringify(justDeliver));
	};

	const btnNoteDeliv = () => setNoteDeliv(!noteDeliv);

	const handleDelivComplete = (e) => {
		setDeliverNote(e.target.value);
		const completeDeliver = {
			...addressDeliver,
			address: deliver,
			note: deliverNote,
		};

		setAddressDeliver(completeDeliver);
		// console.log("addressDeliver", addressDeliver);
		props.sendOrder("deliveryAddress", JSON.stringify(completeDeliver));
	};

	const handleCopyAddress = (e) => {
		setDeliver(pickup);
		setDeliverNote(pickupNote);
		const completeAddress = {
			...addressPickUp,
			address: pickup,
			note: pickupNote,
		};

		setAddressDeliver(addressPickUp);
		props.sendOrder("deliveryAddress", JSON.stringify(completeAddress));
	};
	// console.log('addressDeliver', addressDeliver)

	const sumTotal = () => {
		const total = estimate + admin_charge + delivery_fee;
		props.sendOrder("totalPrice", total.toString());
	};


	const notifyAddress = () =>
		toast("🦄 please input your address", {
			position: "top-right",
			autoClose: 5000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
		});

	const okNext = () => {
		if (pickup === "" && deliver === "") {
			notifyAddress();
		} else if (deliver === "") {
			notifyAddress();
		} else if (pickup === "") {
			notifyAddress();
		} else {
			props.next(2);
		}
	};
	// console.log("pickup", pickup);
	// console.log("deliver", deliver);

	return (
		<div className="setadd-container">
			<Card className="setadd-card">
				<div className="card-title-add">
					<Card.Title>Pick up</Card.Title>
				</div>
				<Form role="search" className="search-laundry">
					<i className="loc">
						<HiLocationMarker />
					</i>
					<FormControl
						className="search-input"
						type="text"
						placeholder="Type your laundry pick up address"
						onChange={handlePickUp}
					/>
				</Form>
				<Button
					className="note-button"
					variant="light"
					value="btnPickUp"
					onClick={btnNotePickUp}
				>
					<FaPen />
					<p>Note</p>
				</Button>
				{notePickUp ? (
					<Form.Control
						className="add-notes-start"
						as="textarea"
						rows={3}
						placeholder="Example: Please pick up and deliver at 10 am"
						onChange={handlePickUpComplete}
					/>
				) : null}

				<div className="card-title-deliver">
					<Card.Title>Deliver to</Card.Title>
				</div>
				<Form.Check
					className="same-address"
					type="checkbox"
					label="Same with Pick up Address"
					onChange={handleCopyAddress}
					onClick={handleCheck}
				/>
				{sameAddress ? null : (
					<>
						<Form role="search" className="search-laundry">
							<i className="loc">
								<HiLocationMarker />
							</i>
							<FormControl
								className="search-input"
								type="text"
								placeholder="Type your laundry delivery address"
								onChange={handleDeliv}
							/>
						</Form>
						<Button
							className="note-button"
							variant="light"
							onClick={btnNoteDeliv}
						>
							<FaPen />
							<p>Note</p>
						</Button>
						{noteDeliv ? (
							<Form.Control
								className="add-notes-end"
								as="textarea"
								rows={3}
								placeholder="Example: Please pick up and deliver at 10 am"
								onChange={handleDelivComplete}
							/>
						) : null}
					</>
				)}
			</Card>
			<ToastContainer
				position="top-right"
				autoClose={5000}
				hideProgressBar={false}
				newestOnTop={false}
				closeOnClick
				rtl={false}
				pauseOnFocusLoss
				draggable
				pauseOnHover
			/>
			<div className="btn-setaddress">
				<Button
					className="btn-setaddress-next"
					variant="light"
					onClick={() => {
						okNext();
						sumTotal();
					}}
				>
					Next
					<div className="arrow-icon">
						<AiOutlineArrowRight />
					</div>
				</Button>
			</div>
		</div>
	);
};

export default SetAddress;
