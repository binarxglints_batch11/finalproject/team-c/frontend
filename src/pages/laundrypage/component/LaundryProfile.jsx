import React, { useContext, useEffect, useState } from "react";
import {
	Container,
	Row,
	Col,
	Card,
	ListGroup,
	Button,
	Accordion,
	AccordionContext,
	useAccordionToggle,
} from "react-bootstrap";
import "./LaundryProfile.scss";

import {
	DelivBike,
	UserPic,
	ListBtn,
} from "../../../assets/index.js";

import ReactStars from "react-rating-stars-component";
import { BsChevronUp, BsChevronDown } from "react-icons/bs";
import { Link } from "react-router-dom";

import { useDispatch, useSelector } from "react-redux";
import { PostFavoriteLaundry } from "../../../store/actions/postFavoriteLaundry";
import { DelFavoriteLaundry } from "../../../store/actions/delFavoriteLaundry";
import { GetFavoriteLaundry } from "../../../store/actions/getFavoriteLaundry";
import { GetDataLaundry } from "../../../store/actions/getDataLaundry";


const ContextTerm = ({ eventKey, callback }) => {
	const currentEventKey = useContext(AccordionContext);
	const detail = useSelector((state) => state.DataLaundry.dataLaundry);

	const handleArrow = useAccordionToggle(
		eventKey,
		() => callback && callback(eventKey)
	);

	const isCurrentEventKey = currentEventKey === eventKey;

	return (
		<div onClick={handleArrow}>
			<Card.Title>
				<div className="container-title">
					<div>
						<h5 className="laundryprofile-h5 tnc-h5">
							Terms & Conditions {detail?.name}
						</h5>
					</div>
					<div>{isCurrentEventKey ? <BsChevronDown /> : <BsChevronUp />}</div>
				</div>
			</Card.Title>
		</div>
	);
};

const LaundryProfile = () => {
	const dispatch = useDispatch();
	const detail = useSelector((state) => state.DataLaundry.dataLaundry);
	// const loading = useSelector((state) => state.DataLaundry.loading);
	const laundryId = window.location.pathname.split("/")[2];
	const [click, setClick] = useState(true);
	const fav = useSelector((state) => state.FavoriteLaundry.favLaundry);

	useEffect(() => {
		dispatch(GetFavoriteLaundry());
	}, []);
	// console.log("fav", fav);

	const filterFav = fav.filter((data) => data.laundry.id === laundryId)

	const handleSave = () => {
		dispatch(PostFavoriteLaundry({laundry_id: laundryId}));
		// dispatch(GetFavoriteLaundry());
	}

	const handleDelete = () => {
		dispatch(DelFavoriteLaundry(filterFav[0].id));
		// dispatch(GetFavoriteLaundry());
	}

	useEffect(() => {
		dispatch(GetDataLaundry(laundryId));
	}, []);

	// console.log("detail", detail);
	// console.log("laundrypath", laundryId);

	return (
		<>
			<Container className="laundryprofile-container">
				<Row className="laundryprofile-row">
					<Col className="detail-laundry-col">
						<Card className="detail-laundry">
							<ListGroup>
								<ListGroup.Item className="laundry-overview">
									<Row>
										<Col className="laundry-col-1">
											<img
												className="wash-mach"
												src={`https://washme.gabatch11.my.id/${detail?.image}`}
												alt="washing..."
											/>
										</Col>

										<Col className="laundry-col-2">
											<h4 className="laundryprofile-h4">{detail?.name}</h4>
											<div className="laundry-overview-row">
												<p className="laundry-overview-p">
													{detail?.address?.street}
												</p>
												{/* <p className="laundry-overview-p">0,7 km</p> */}
											</div>
											<p className="laundry-overview-p">
												{detail?.total_services} services
														</p>
											<div className="laundry-overview-row">
												<p className="laundry-overview-p">
													{detail?.pickUpAndDelivery
														? "Pick up & Delivery"
														: null}
												</p>
												<img
													className="deliv-bike"
													src={DelivBike}
													alt="bike"
												/>
											</div>
										</Col>

										<Col className="laundry-col-3">
											<div className="laundry-review-row">
												<ReactStars
													count={1}
													value={1}
													size={15}
													activeColor="#ffd700"
												/>
												<p className="laundry-overview-p" id="star-p">
													{detail?.average_rating}
												</p>
												<p className="laundry-overview-p" id="review-p">
													({detail?.total_review} reviews)
												</p>
											</div>
											<p className="laundry-grey-p">Open 09.00 am - 09.00 pm</p>
											{!filterFav.length ?
												<Button
													className="btn-setorder-list"
													variant="outline-light"
													type="button"
													onClick={() => handleSave()}
												>
													Save Laundry
												<div className="list-btn-icon">
														<img src={ListBtn} alt="list icon btn" />
													</div>
												</Button>
												:
												<Button
													className="btn-setorder-list-after"
													variant="outline-light"
													type="button"
													onClick={() => handleDelete()}
												>
													Laundry Saved
												</Button>
											}
										</Col>
									</Row>
								</ListGroup.Item>

								{detail?.services?.map((item, index) => {
									return (
										<div key={index}>
											{item?.basic ? (
												<ListGroup.Item className="laundry-services laundry-services-large">
													<h4 className="laundry-services-h4">{item?.name}</h4>
													<Row>
														<i className="border-img">
															<img
																className="icon-circle"
																src={`https://washme.gabatch11.my.id/${item.basic.image}`}
																alt="wash-iron"
															/>
														</i>

														<Col className="services-col-2">
															<p className="laundry-services-p">
																{`${item.name} ${item.basic.name}`}
															</p>
															{item?.basic?.byItem ? (
																<p className="laundry-grey-p">
																	Rp {item.basic.byItem.minimumPrice}/item
																</p>
															) : null}
															{item?.basic?.byWeight ? (
																<p className="laundry-grey-p">
																	Rp {item.basic.byWeight.price}/kilogram
																</p>
															) : null}
														</Col>

														<Col className="services-col-3">
															<p className="laundry-grey-p">
																{item.basic.days} days working
															</p>
															<Link
																to={{
																	pathname: `/laundry/${detail?.id}/order/${item.id}/${item.basic.id}`,
																	state: {
																		id: detail.id,
																		laundry: detail.name,
																		serviceId: item.id,
																		service: item.basic,
																		name: item.name,
																	},
																}}
															>
																<Button>Order</Button>
															</Link>
														</Col>
													</Row>
												</ListGroup.Item>
											) : null}
											{item?.express ? (
												<ListGroup.Item className="laundry-services">
													<Row>
														<i className="border-img">
															<img
																className="icon-circle"
																src={`https://washme.gabatch11.my.id/${item.express.image}`}
																alt="wash-iron"
															/>
														</i>

														<Col className="services-col-2">
															<p className="laundry-services-p">
																{`${item.name} ${item.express.name}`}
															</p>
															{item?.express?.byItem ? (
																<p className="laundry-grey-p">
																	Rp {item.express.byItem.minimumPrice}/item
																</p>
															) : null}
															{item?.basic?.byItem ? (
																<p className="laundry-grey-p">
																	Rp {item.express.byWeight.price}/kilogram
																</p>
															) : null}
														</Col>

														<Col className="services-col-3">
															<p className="laundry-grey-p">
																{item.express.days} days working
															</p>

															<Link
																to={{
																	pathname: `/laundry/${detail?.id}/order/${item.id}/${item.express.id}`,
																	state: {
																		id: detail.id,
																		laundry: detail.name,
																		serviceId: item.id,
																		service: item.express,
																		name: item.name,
																	},
																}}
															>
																<Button>Order</Button>
															</Link>
														</Col>
													</Row>
												</ListGroup.Item>
											) : null}
										</div>
									);
								})}
							</ListGroup>
						</Card>
					</Col>

					<Col className="tnc-reviews-col">
						<Card className="tnc">
							<Accordion className="tnc-accordion">
								<Card>
									<Accordion.Toggle
										as={Card.Header}
										eventKey="1"
										className="tnc-header"
										variant="transparent"
									>
										<ContextTerm eventKey="1" />
									</Accordion.Toggle>
									<Accordion.Collapse eventKey="1" className="tnc-content">
										<Card.Body>
											<p className="laundry-grey-p">
												Please read these terms and conditions of use carefully
												before accessing, using or obtaining any materials,
												information, products or services. By accessing, the{" "}
												{detail?.name} website, mobile or tablet application, or
												any other feature or other {detail?.name} platform
												(collectively "Our Website") you agree to be bound by
												these terms and conditions ("Terms") and our Privacy
												Policy.
											</p>
										</Card.Body>
									</Accordion.Collapse>
								</Card>
							</Accordion>
						</Card>

						<Card className="reviews">
							<ListGroup>
								<ListGroup.Item>
									<div className="review-header-row">
										<div>
											<h5 className="laundryprofile-h5">Reviews</h5>
										</div>
										<div>
											<Row>
												<ReactStars
													count={1}
													value={1}
													size={15}
													activeColor="#ffd700"
												/>
												<p className="laundry-overview-p" id="star-p">
													{detail?.average_rating}
												</p>
												<p className="laundry-overview-p" id="review-p">
													({detail?.total_review} reviews)
												</p>
											</Row>
										</div>
									</div>
								</ListGroup.Item>

								<ListGroup.Item>
									<Row className="review-user-row">
										<img className="icon-circle" src={UserPic} alt="user" />
										<Row className="review-user-content">
											<Col id="review-col1">
												<p className="reviews-user-p">Rudi</p>
												<p className="reviews-feedback-p">
													Really nice service!
												</p>
											</Col>
											<div>
												<div className="review-stars">
													<ReactStars
														count={5}
														value={5}
														size={15}
														activeColor="#ffd700"
													/>
												</div>
												<div className="review-date">
													<p className="laundry-grey-p">17-09-2020</p>
												</div>
											</div>
										</Row>
									</Row>
								</ListGroup.Item>

								<ListGroup.Item>
									<Row className="review-user-row">
										<img className="icon-circle" src={UserPic} alt="user" />
										<Row className="review-user-content">
											<Col id="review-col1">
												<p className="reviews-user-p">Iren</p>
												<p className="reviews-feedback-p">
													Nice service and result
												</p>
											</Col>
											<div>
												<div className="review-stars">
													<ReactStars
														count={5}
														value={5}
														size={15}
														activeColor="#ffd700"
													/>
												</div>
												<div className="review-date">
													<p className="laundry-grey-p">17-09-2020</p>
												</div>
											</div>
										</Row>
									</Row>
								</ListGroup.Item>

								<ListGroup.Item>
									<Row className="review-user-row">
										<img className="icon-circle" src={UserPic} alt="user" />
										<Row className="review-user-content">
											<Col id="review-col1">
												<p className="reviews-user-p">Mala</p>
												<p className="reviews-feedback-p">
													Nice packaging when delivery
												</p>
											</Col>
											<div>
												<div className="review-stars">
													<ReactStars
														count={5}
														value={5}
														size={15}
														activeColor="#ffd700"
													/>
												</div>
												<div className="review-date">
													<p className="laundry-grey-p">17-09-2020</p>
												</div>
											</div>
										</Row>
									</Row>
								</ListGroup.Item>

								<ListGroup.Item>
									<Row className="review-user-row">
										<img className="icon-circle" src={UserPic} alt="user" />
										<Row className="review-user-content">
											<Col id="review-col1">
												<p className="reviews-user-p">Nisa</p>
												<p className="reviews-feedback-p">Good service</p>
											</Col>
											<div>
												<div className="review-stars">
													<ReactStars
														count={5}
														value={5}
														size={15}
														activeColor="#ffd700"
													/>
												</div>
												<div className="review-date">
													<p className="laundry-grey-p">17-09-2020</p>
												</div>
											</div>
										</Row>
									</Row>
								</ListGroup.Item>

								<ListGroup.Item>
									<Row className="review-user-row">
										<img className="icon-circle" src={UserPic} alt="user" />
										<Row className="review-user-content">
											<Col id="review-col1">
												<p className="reviews-user-p">Bila</p>
												<p className="reviews-feedback-p">
													Will order at this laundry again
												</p>
											</Col>
											<div>
												<div className="review-stars">
													<ReactStars
														count={5}
														value={5}
														size={15}
														activeColor="#ffd700"
													/>
												</div>
												<div className="review-date">
													<p className="laundry-grey-p">17-09-2020</p>
												</div>
											</div>
										</Row>
									</Row>
								</ListGroup.Item>
							</ListGroup>
						</Card>
					</Col>
				</Row>
			</Container>
		</>
	);
};

export default LaundryProfile;
