import React from 'react';
import './LaundryProfilePage.scss';
import LaundryProfile from './component/LaundryProfile';
import Footer from '../../common/footer/Footer';
import NavbarComponent from '../../common/navbar/Navbar';
import FilterBar from '../../common/filterbar/FilterBar';

function LaundryProfilePage() {
  return (
    <div className="laundryprofile">
      <NavbarComponent />
      <LaundryProfile />
      <Footer />
    </div>
  );
}

export default LaundryProfilePage;
