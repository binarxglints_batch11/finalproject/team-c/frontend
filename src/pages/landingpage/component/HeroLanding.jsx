import React from "react";
import { Link } from "react-router-dom";
import { Container, Button, Card } from "react-bootstrap";
import { useSelector } from "react-redux";
import "./HeroLanding.scss";
import Login from "./Login";
import SignUp from "./Signup";

function HeroLanding() {
	const authNavbar = useSelector((state) => state.authNavbar);

	return (
		<Container className="hero-landing">
			{authNavbar.login ? (
				<Login />
			) : authNavbar.register ? (
				<SignUp />
			) : (
				<Card className="card-landing">
					<Card.Body className="cardby-landing">
						<div className="hero-body">
							<Card.Title className="hero-title">
								No more difficulties to find laundry
							</Card.Title>
							<Card.Text className="hero-text">
								We help you to find your ideal laundry place anywhere and
								anytime
							</Card.Text>
							<Link to={"/services/all"}>
								<Button
									className="btn-find"
									style={{
										marginTop: "1rem",
										width: "15rem",
										backgroundColor: "#FF415B",
										borderColor: "#FF415B",
										color: "#FFFFFF",
									}}
									variant="light"
								>
									Find your ideal laundry now
								</Button>
							</Link>
						</div>
					</Card.Body>
				</Card>
			)}
		</Container>
	);
}

export default HeroLanding;
