import React, { Component } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';
import { Redirect, withRouter } from 'react-router-dom';
import { isEmail, isStrongPassword } from 'validator';
import { connect } from 'react-redux';
import { register } from '../../../store/actions/auth';
import './SignupLogin.scss';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { compose } from 'redux';

let pass = '';

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const email = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        This is not a valid email.
      </div>
    );
  }
};

const vpassword = (value) => {
  pass = value;
  if (!isStrongPassword(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        The password must be at least 8 characters long contain a number and an uppercase letter.
      </div>
    );
  }
};

const confirmPassword = (value) => {
  if (value !== pass) {
    return (
      <div className="alert alert-danger" role="alert">
        Password confirmation must be the same with password
      </div>
    );
  }
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangeConfirmPassword = this.onChangeConfirmPassword.bind(this);
    this.state = {
      email: '',
      password: '',
      confirmpassword: '',
      successful: false,
    };
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value,
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value,
    });
  }

  onChangeConfirmPassword(e) {
    this.setState({
      confirmpassword: e.target.value,
    });
  }

  handleRegister(e) {
    e.preventDefault();

    this.setState({
      successful: false,
    });

    this.form.validateAll();

    if (this.checkBtn.context._errors.length === 0) {
      this.props
        .dispatch(register(this.state.email, this.state.password, this.state.confirmpassword))
        .then(() => {
          this.successfull();
          this.setState({
            successful: true,
          });
        })
        .catch(() => {
          this.unSuccessfull();
          this.setState({
            successful: false,
          });
        });
    }
  }

  successfull = () => {
    toast('🦄 Please check your email for verification', {
      position: 'top-left',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };
  unSuccessfull = () => {
    toast('Register Failed! Email has been used.', {
      position: 'top-left',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };
  // notify = () => toast(' Please check your email for verification ');

  render() {
    const { isLoggedIn } = this.props;

    if (isLoggedIn) {
      return <Redirect to="/" />;
    }

    return (
      <div className="col-md-12">
        {!this.state.successful ? (
          <div className="card card-container">
            <Form
              onSubmit={this.handleRegister}
              ref={(c) => {
                this.form = c;
              }}
            >
              <div>
                <div className="form-group">
                  <label htmlFor="text" className="label-signup">
                    Sign Up
                  </label>
                </div>
                <div className="form-group">
                  <label htmlFor="email" className="label-emailpass">
                    E-mail
                  </label>
                  <Input type="text" className="form-control" name="email" placeholder="example : washme@mail.com" value={this.state.email} onChange={this.onChangeEmail} validations={[required, email]} />
                </div>

                <div className="form-group">
                  <label htmlFor="password" className="label-emailpass">
                    Create a password
                  </label>
                  <Input type="password" className="form-control" name="password" placeholder="example : Washme2021!" value={this.state.password} onChange={this.onChangePassword} validations={[required, vpassword]} />
                </div>

                <div className="form-group">
                  <label htmlFor="password" className="label-emailpass">
                    Confirm password
                  </label>
                  <Input type="password" className="form-control" name="password" placeholder="example : Washme2021!" value={this.state.confirmpassword} onChange={this.onChangeConfirmPassword} validations={[required, confirmPassword]} />
                </div>

                <div className="button-group">
                  {/* <a className="btn btn-google" href="https://washme.gabatch11.my.id/google">
                    <img className="btn-img" src="https://img.icons8.com/color/50/000000/google-logo.png" alt="Google login Logo" />
                    Sign up with google
                  </a> */}
                  <button className="btn btn-signup">Sign Up</button>
                </div>
              </div>

              <CheckButton
                style={{ display: 'none' }}
                ref={(c) => {
                  this.checkBtn = c;
                }}
              />
            </Form>
          </div>
        ) : (
          <div
            style={{
              width: '32rem',
              minHeight: '25.5rem',
              marginTop: '0rem',
              padding: '2.5rem',
              background: 'transparent',
              boxSizing: 'border-box',
              borderRadius: '10px',
            }}
          >
            {/* {message && (
              <Toast className={this.state.successful ? 'alert alert-success' : 'alert alert-danger'} role="alert">
                <Toast.Body>{message}</Toast.Body>
              </Toast>
            )} */}
          </div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { message } = state.message;
  return {
    message,
  };
}

export default compose(withRouter, connect(mapStateToProps))(Register);
