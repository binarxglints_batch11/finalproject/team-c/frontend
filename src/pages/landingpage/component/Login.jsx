import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { login } from '../../../store/actions/auth';
import './SignupLogin.scss';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { isEmail, isStrongPassword } from 'validator';

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const email = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        This is not a valid email.
      </div>
    );
  }
};

const wrongPassword = (value) => {
  if (!isStrongPassword(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        You must write the wrong password
      </div>
    );
  }
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);

    this.state = {
      email: '',
      password: '',
      successfull: false,
      loading: false,
    };
  }

  onChangeEmail(e) {
    this.setState({
      email: e.target.value,
    });
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value,
    });
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      loading: true,
    });

    this.setState({
      successfull: false,
    });

    this.form.validateAll();

    const { dispatch, history } = this.props;

    if (this.checkBtn.context._errors.length === 0) {
      dispatch(login(this.state.email, this.state.password))
        .then(() => {
          this.successfull();
          history.push('/homepage');
          // window.location.reload();
          this.setState({
            successful: true,
          });
        })
        .catch((e) => {
          console.log('ini e', e);
          this.unSuccessfull();
          this.setState({
            loading: false,
            successful: false,
          });
        });
    } else {
      this.setState({
        loading: false,
      });
    }
  }

  successfull = () => {
    toast('🦄 Welcome to wash me! ', {
      position: 'top-left',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };
  unSuccessfull = () => {
    toast('Login failed, please check your email/password', {
      position: 'top-left',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  // notify = () => toast(' Welcome to wash me! 🦄');

  render() {
    const { isLoggedIn } = this.props;

    if (isLoggedIn) {
      return <Redirect to="/Homepage" />;
    }

    return (
      <div className="col-md-12">
        {!this.state.successful ? (
          <div className="card card-container">
            <Form
              onSubmit={this.handleLogin}
              ref={(c) => {
                this.form = c;
              }}
            >
              <div className="form-group">
                <label htmlFor="text" className="label-signup">
                  Log in
                </label>
              </div>
              <div className="form-group">
                <label htmlFor="email" className="label-emailpass">
                  E-mail
                </label>
                <Input type="text" className="form-control" name="email" placeholder="example : washme@mail.com" value={this.state.email} onChange={this.onChangeEmail} validations={[required, email]} />
              </div>

              <div className="form-group">
                <label htmlFor="password" className="label-emailpass">
                  Password
                </label>
                <Input type="password" className="form-control" name="password" placeholder="example : Washme2021!" value={this.state.password} onChange={this.onChangePassword} validations={[required, wrongPassword]} />
              </div>
              <div className="button-group">
                {/* <a className="btn btn-google" href="https://washme.gabatch11.my.id/google">
                  <img className="btn-img" src="https://img.icons8.com/color/50/000000/google-logo.png" alt="Google login Logo" />
                  Login with google
                </a> */}
                <button className="btn btn-signup" disabled={this.state.loading}>
                  {this.state.loading && <span className="spinner-border spinner-border-sm"></span>}
                  Login
                </button>
              </div>

              <CheckButton
                style={{ display: 'none' }}
                ref={(c) => {
                  this.checkBtn = c;
                }}
              />
            </Form>
          </div>
        ) : (
          <div>salah</div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { isLoggedIn } = state.auth;
  const { message } = state.message;

  return {
    isLoggedIn,
    message,
  };
}

export default compose(withRouter, connect(mapStateToProps))(Login);
