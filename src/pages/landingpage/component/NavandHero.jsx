import React from 'react'
import NavbarComponent from '../../../common/navbar/Navbar'
import HeroLanding from './HeroLanding'
import './NavandHero.scss'

function NavandHero() {
    return (
        <div className='hero-bg'>
            <NavbarComponent />
            <HeroLanding />
        </div>
    )
}

export default NavandHero
