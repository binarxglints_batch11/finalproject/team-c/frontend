import React from "react";
import { Link } from "react-router-dom";
import "./LandingPageComponents.scss";
import { Container, Row, Col, Button } from "react-bootstrap";

const LandingPageComponents = () => {
	return (
		<div>
			<Container className="landing-comp">
				<Row>
					<Col className="landing-item">
						<img
							className="icon-item"
							src="https://i.ibb.co/CMgZHCf/icon-left.png"
							alt="icon-left"
						/>
						<h3>Find ideal laundry easy</h3>
						<p>
							See laundries around you with variety of services. Once you find
							one that fit you, set your order!
						</p>
					</Col>
					<Col className="landing-item">
						<img
							className="icon-item"
							src="https://i.ibb.co/zQJKrnx/icon-middle.png"
							alt="icon-middle"
						/>
						<h3>Set your order</h3>
						<p>
							Set the order, then we will pick up and deliver at your home.
							Enjoy your time and we will process your laundry!
						</p>
					</Col>
					<Col className="landing-item">
						<img
							className="icon-item"
							src="https://i.ibb.co/Df6nhbz/icon-right.png"
							alt="icon-right"
						/>
						<h3>Now all are clean!</h3>
						<p>
							All clothes are now clean and ready to wear. Wash me is your
							laundry solution. Washing made easy!
						</p>
					</Col>
				</Row>
				<Link to={"/services/all"}>
					<Button
						className="btn-landingpage"
						variant="light"
						style={{
							backgroundColor: "#FF415B",
							borderColor: "#FF415B",
							fontWeight: "500",
							color: "#FFFFFF",
							width: "16rem",
							marginTop: "0.9rem",
							marginBottom: "5rem",
						}}
					>
						Learn how Wash Me works
					</Button>
				</Link>
				<h2>Why Wash Me?</h2>
				<Row>
					<Col className="landing-item2">
						<img
							className="frame"
							src="https://i.ibb.co/y4SZvM1/frame.png"
							alt="frame wash me"
						/>
					</Col>
					<Col className="landing-item2">
						<div>
							<div className="icon-title">
								<img
									className="icon-item2"
									src="https://i.ibb.co/z5Sgy1b/weight.png"
									alt="shield"
								/>
								<h6>Set by item or by weight!</h6>
							</div>
							<p>
								You can decide to set your clothes by item or by weight. With
								wash me, everything is possible.
							</p>
						</div>
						<div>
							<div className="icon-title">
								<img
									className="icon-item2"
									src="https://i.ibb.co/mq40cVG/time.png"
									alt="time"
								/>
								<h6>Enjoy your time!</h6>
							</div>
							<p>
								Your laundry will be processed, just wait in your home and enjoy
								your time. With wash me, your time is efficient.
							</p>
						</div>
						<div>
							<div className="icon-title">
								<img
									className="icon-item2"
									src="https://i.ibb.co/MVHFxNn/shield.png"
									alt="weight"
								/>
								<h6>Now all are clean!</h6>
							</div>
							<p className="bottom-p">
								No need to worry, we will pick up and deliver your laundry. With
								wash me, everything is easy.
							</p>
						</div>
					</Col>
				</Row>
				<Link to={"/about-us"}>
					<Button
						className="btn-landingpage"
						variant="light"
						style={{
							backgroundColor: "#FF415B",
							borderColor: "#FF415B",
							fontWeight: "500",
							color: "#FFFFFF",
							width: "8rem",
							marginTop: "0.9rem",
							marginBottom: "5rem",
						}}
						target="_blank"
						rel="noreferrer"
					>
						See about us
					</Button>
				</Link>
				<h2>Our Services</h2>
				<Row>
					<Col className="landing-item3">
						<img
							className="icon-services"
							src="https://i.ibb.co/FHG2YBx/washing-only.png"
							alt="wash-only"
						/>
						<h3>Wash Only</h3>
						<p>
							Wash only is one of our services that will only wash your clothes
							at an affordable price.
						</p>
					</Col>
					<Col className="landing-item3">
						<img
							className="icon-services"
							src="https://i.ibb.co/F7rzjqD/iron-only.png"
							alt="iron-only"
						/>
						<h3>Iron Only</h3>
						<p>
							Iron only is one of our services that will only iron your clothes
							at an affordable price.
						</p>
					</Col>
					<Col className="landing-item3">
						<img
							className="icon-services"
							src="https://i.ibb.co/RCwm8N6/washing-iron.png"
							alt="wash-iron"
						/>
						<h3>Wash & Iron</h3>
						<p>
							Wash and Iron is one of our services that will wash and iron your
							clothes at an affordable price.
						</p>
					</Col>
				</Row>
				<Row>
					<Col className="landing-item3">
						<img
							className="icon-services"
							src="https://i.ibb.co/9hsyQ7r/dry-clean.png"
							alt="dry-clean"
						/>
						<h3>Dry Clean</h3>
						<p>
							Dry Clean is one of our services that will dry clean your clothes
							at an affordable price.
						</p>
					</Col>
					<Col className="landing-item3">
						<img
							className="icon-services"
							src="https://i.ibb.co/thdbFMC/shoes.png"
							alt="shoes"
						/>
						<h3>Shoes</h3>
						<p>
							Shoes is one of our services that will wash your shoes at an
							affordable price.
						</p>
					</Col>
					<Col className="landing-item3">
						<img
							className="icon-services"
							src="https://i.ibb.co/mDQZsr2/house-hold.png"
							alt="house-hold"
						/>
						<h3>Household</h3>
						<p>
							Household is one of our services that will wash your household at
							an affordable price.
						</p>
					</Col>
				</Row>
				<Link to={"/services/all"}>
					<Button
						className="btn-landingpage"
						variant="light"
						style={{
							backgroundColor: "#FF415B",
							borderColor: "#FF415B",
							fontWeight: "500",
							color: "#FFFFFF",
							width: "16rem",
							marginTop: "0.9rem",
							marginBottom: "5rem",
						}}
					>
						Learn how to order services
					</Button>
				</Link>
			</Container>
		</div>
	);
};

export default LandingPageComponents;
