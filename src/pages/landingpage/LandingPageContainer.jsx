import React from 'react';
import LandingPageComponents from './component/LandingPageComponents';
import AdvBanner from '../../common/banner/AdvBanner';
import Footer from '../../common/footer/Footer';
import NavandHero from './component/NavandHero';

const LandingPageContainer = () => {
  return (
    <div>
      <NavandHero />
      <LandingPageComponents />
      <AdvBanner />
      <Footer />
    </div>
  );
};

export default LandingPageContainer;
