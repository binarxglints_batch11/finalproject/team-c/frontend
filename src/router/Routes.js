import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
// import { Provider } from 'react-redux';
// import store from '../store';
import LandingPageContainer from '../pages/landingpage/LandingPageContainer';
import Homepage from '../pages/homepage/Homepage';
import Services from '../pages/servicespage/Services';
import AboutUsPage from '../pages/aboutpage/AboutUsPage';
import Contact from '../pages/contactpage/Contact';
import Error404 from '../pages/pagenotfound/Error404';
import LaundryProfilePage from '../pages/laundrypage/LaundryProfilePage';
import Orderpage from '../pages/orderpage/Orderpage';
import UserPage from '../pages/userpage/UserPage';

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <LandingPageContainer />
        </Route>
        <Route path="/homepage" exact>
          <Homepage />
        </Route>
        <Route path="/services/:id" exact>
          <Services />
        </Route>
        <Route path="/about-us" exact>
          <AboutUsPage />
        </Route>
        <Route path="/contact" exact>
          <Contact />
        </Route>
        <Route path="/laundry/:id" exact>
          <LaundryProfilePage />
        </Route>
        <Route path="/laundry/:id/order/:idService/:idLevel" exact>
          <Orderpage />
        </Route>
        <Route path="/profile/:id" exact>
          <UserPage />
        </Route>
        <Route path="/*">
          <Error404 />
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;
