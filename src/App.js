import './App.css';
import Routes from './router/Routes';
import { Provider } from 'react-redux';
import store from './store';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div className="App">
      <Provider store={store}>
        <Routes />
      </Provider>
      <ToastContainer position="top-left" />
    </div>
  );
}

export default App;
