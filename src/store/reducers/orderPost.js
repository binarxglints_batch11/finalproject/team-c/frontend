import {
	POST_ORDER_BEGIN,
	POST_ORDER_SUCCESS,
	POST_ORDER_FAILED,
} from "../actions/actionTypes";

const initialState = {
	loading: false,
	error: null,
	detailOrder: {}
};

const CreateOrder = (state = initialState, action) => {
	const { type, payload, error } = action;
	switch (type) {
		default:
			return {
				...state,
			};
		case POST_ORDER_BEGIN:
			return {
				...state,
				loading: true,
				error: null,
			};
		case POST_ORDER_SUCCESS:
			return {
				...state,
				loading: false,
				detailOrder: payload,
				error: null,
			};
		case POST_ORDER_FAILED:
			return {
				...state,
				loading: false,
				error: error,
			};
	}
};
export default CreateOrder;
