import {
	GET_FAVORITE_LAUNDRY_BEGIN,
	GET_FAVORITE_LAUNDRY_SUCCESS,
	GET_FAVORITE_LAUNDRY_FAILED,
} from "../actions/actionTypes";

const initialState = {
	loading: false,
	error: null,
	favLaundry: [],
};

const FavoriteLaundry = (state = initialState, action) => {
	const { type, payload, error } = action;
	switch (type) {
		default:
			return {
				...state,
			};
		case GET_FAVORITE_LAUNDRY_BEGIN:
			return {
				...state,
				loading: true,
				error: null,
			};
		case GET_FAVORITE_LAUNDRY_SUCCESS:
			return {
				...state,
				loading: false,
				favLaundry: payload,
				error: null,
			};
		case GET_FAVORITE_LAUNDRY_FAILED:
			return {
				...state,
				loading: false,
				error: error,
				favLaundry: [],
			};
	}
};
export default FavoriteLaundry;
