import { combineReducers } from "redux";
import auth from "./auth";
import message from "./message";
import authNavbar from "./authNavbar";
import UserProfile from "./GetUserProfile";
import DataLaundry from "./getDataLaundry";
import laundries from "./laundries";
import CreateOrder from "./orderPost";
import OrderDetail from "./orderGet";
import OrderItem from "./orderSet";
import OrderFinishDetail from "./orderFinishGet";
import FavoriteLaundry from "./getFavoriteLaundry";

export default combineReducers({
	auth,
	message,
	authNavbar,
	UserProfile,
	DataLaundry,
	laundries,
	CreateOrder,
	OrderDetail,
	OrderItem,
	OrderFinishDetail,
	FavoriteLaundry
});

