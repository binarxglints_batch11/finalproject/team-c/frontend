import { SET_ORDER_VALUE_SUCCESS } from '../actions/actionTypes';

const orderItem = {
  // laundry_id: '',
  // servicelevel_id: null,
  // servicelist_id: null,
};

export default function OrderItem (state = orderItem, action) {
  const { type, payload } = action;

  switch (type) {
    case SET_ORDER_VALUE_SUCCESS:
      return action.payload
    
    default:
      return state;
  }
}
