import {
	GET_USER_PROFILE_BEGIN,
	GET_USER_PROFILE_SUCCESS,
	GET_USER_PROFILE_FAILED,
} from "../actions/actionTypes";

const initialState = {
	loading: false,
	error: null,
	profile: [],
};

const UserProfile = (state = initialState, action) => {
	const { type, payload, error } = action;
	switch (type) {
		default:
			return {
				...state,
			};
		case GET_USER_PROFILE_BEGIN:
			return {
				...state,
				loading: true,
				error: null,
			};
		case GET_USER_PROFILE_SUCCESS:
			return {
				...state,
				loading: false,
				profile: payload,
				error: null,
			};
		case GET_USER_PROFILE_FAILED:
			return {
				...state,
				loading: false,
				error: error,
				profile:[]
			};
	}
};
export default UserProfile;
