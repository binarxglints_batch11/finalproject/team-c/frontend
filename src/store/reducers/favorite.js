import { POST_FAVORITE_LAUNDRY_BEGIN, POST_FAVORITE_LAUNDRY_SUCCESS, POST_FAVORITE_LAUNDRY_FAILED } from '../actions/actionTypes';

const initialState = {
  loading: false,
  error: null,
  detailOrder: {},
};

const favoriteLaundry = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case POST_FAVORITE_LAUNDRY_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case POST_FAVORITE_LAUNDRY_SUCCESS:
      return {
        ...state,
        loading: false,
        detailOrder: payload,
        error: null,
      };
    case POST_FAVORITE_LAUNDRY_FAILED:
      return {
        ...state,
        loading: false,
        error: error,
      };
  }
};
export default favoriteLaundry;
