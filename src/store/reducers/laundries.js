import { GET_LAUNDRIES_BEGIN, GET_LAUNDRIES_SUCCESS, GET_LAUNDRIES_FAILED } from '../actions/actionTypes';

const initialState = {
  loading: false,
  error: null,
  allLaundry: { data: [] },
};

const laundries = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_LAUNDRIES_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case GET_LAUNDRIES_SUCCESS:
      return {
        ...state,
        loading: false,
        allLaundry: payload,
        error: null,
      };
    case GET_LAUNDRIES_FAILED:
      return {
        ...state,
        loading: false,
        error: error,
      };
  }
};
export default laundries;
