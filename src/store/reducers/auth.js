import { POST_REGISTER_SUCCESS, POST_REGISTER_FAILED, POST_LOGIN_SUCCESS, POST_LOGIN_FAILED, LOGOUT } from '../actions/actionTypes';

const user = JSON.parse(localStorage.getItem('user'));

const initialState = user ? { isLoggedIn: true, user } : { isLoggedIn: false, user: null };

export default function auth(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case POST_REGISTER_SUCCESS:
      return {
        ...state,
        isLoggedIn: false,
      };
    case POST_REGISTER_FAILED:
      return {
        ...state,
        isLoggedIn: false,
      };
    case POST_LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        user: payload.user,
      };
    case POST_LOGIN_FAILED:
      return {
        ...state,
        isLoggedIn: false,
        user: null,
      };
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        user: null,
      };
    default:
      return state;
  }
}
