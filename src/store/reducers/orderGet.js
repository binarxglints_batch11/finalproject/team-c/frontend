import {
	GET_ORDER_BEGIN,
	GET_ORDER_SUCCESS,
	GET_ORDER_FAILED,
} from "../actions/actionTypes";

const initialState = {
	loading: false,
	error: null,
	userOrder: []
};

const OrderDetail = (state = initialState, action) => {
	const { type, payload, error } = action;
	switch (type) {
		default:
			return {
				...state,
			};
		case GET_ORDER_BEGIN:
			return {
				...state,
				loading: true,
				error: null,
			};
		case GET_ORDER_SUCCESS:
			return {
				...state,
				loading: false,
				userOrder: payload,
				error: null,
			};
		case GET_ORDER_FAILED:
			return {
				...state,
				loading: false,
				error: error,
			};
	}
};
export default OrderDetail;
