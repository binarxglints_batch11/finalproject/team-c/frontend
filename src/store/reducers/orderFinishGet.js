import {
	GET_ORDER_FINISH_BEGIN,
	GET_ORDER_FINISH_SUCCESS,
	GET_ORDER_FINISH_FAILED,
} from "../actions/actionTypes";

const initialState = {
	loading: false,
	error: null,
	userOrderFinish: []
};

const OrderFinishDetail = (state = initialState, action) => {
	const { type, payload, error } = action;
	switch (type) {
		default:
			return {
				...state,
			};
		case GET_ORDER_FINISH_BEGIN:
			return {
				...state,
				loading: true,
				error: null,
			};
		case GET_ORDER_FINISH_SUCCESS:
			return {
				...state,
				loading: false,
				userOrderFinish: payload,
				error: null,
			};
		case GET_ORDER_FINISH_FAILED:
			return {
				...state,
				loading: false,
				error: error,
			};
	}
};
export default OrderFinishDetail;
