import {
	GET_DATA_LAUNDRY_BEGIN,
	GET_DATA_LAUNDRY_SUCCESS,
	GET_DATA_LAUNDRY_FAILED,
} from "../actions/actionTypes";

const initialState = {
	loading: false,
	error: null,
	dataLaundry: [],
};

const DataLaundry = (state = initialState, action) => {
	const { type, payload, error } = action;
	switch (type) {
		default:
			return {
				...state,
			};
		case GET_DATA_LAUNDRY_BEGIN:
			return {
				...state,
				loading: true,
				error: null,
			};
		case GET_DATA_LAUNDRY_SUCCESS:
			return {
				...state,
				loading: false,
				dataLaundry: payload,
				error: null,
			};
		case GET_DATA_LAUNDRY_FAILED:
			return {
				...state,
				loading: false,
				error: error,
				dataLaundry: []
			};
	}
};
export default DataLaundry;
