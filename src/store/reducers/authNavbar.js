import { SET_LOGIN_CHANGE, SET_REGISTER_CHANGE } from '../actions/actionTypes';

const initialState = {
  login: false,
  register: false,
};

export default function authNavbar(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case SET_LOGIN_CHANGE:
      return {
        ...state,
        login: payload,
        register: false,
      };
    case SET_REGISTER_CHANGE:
      return {
        ...state,
        register: payload,
        login: false,
      };

    //   case LOGOUT:
    //     return {
    //       ...state,
    //       isLoggedIn: false,
    //       user: null,
    //     };
    default:
      return state;
  }
}
