export const POST_REGISTER_BEGIN = 'POST_REGISTER_BEGIN';
export const POST_REGISTER_SUCCESS = 'POST_REGISTER_SUCCESS';
export const POST_REGISTER_FAILED = 'POST_REGISTER_FAILED';

export const POST_LOGIN_BEGIN = 'POST_LOGIN_BEGIN';
export const POST_LOGIN_SUCCESS = 'POST_LOGIN_SUCCESS';
export const POST_LOGIN_FAILED = 'POST_LOGIN_FAILED';
export const LOGOUT = 'LOGOUT';

export const SET_MESSAGE = 'SET_MESSAGE';
export const CLEAR_MESSAGE = 'CLEAR_MESSAGE';

export const SET_LOGIN_CHANGE = 'SET_LOGIN_CHANGE';
export const SET_REGISTER_CHANGE = 'SET_REGISTER_CHANGE';

export const GET_USER_PROFILE_BEGIN = 'GET_USER_PROFILE_BEGIN';
export const GET_USER_PROFILE_SUCCESS = 'GET_USER_PROFILE_SUCCESS';
export const GET_USER_PROFILE_FAILED = 'GET_USER_PROFILE_FAILED';

export const PUT_USER_PROFILE_BEGIN = 'PUT_USER_PROFILE_BEGIN';
export const PUT_USER_PROFILE_SUCCESS = 'PUT_USER_PROFILE_SUCCESS';
export const PUT_USER_PROFILE_FAILED = 'PUT_USER_PROFILE_FAILED';

export const GET_DATA_LAUNDRY_BEGIN = 'GET_LAUNDRY_DATA_BEGIN';
export const GET_DATA_LAUNDRY_SUCCESS = 'GET_LAUNDRY_DATA_SUCCESS';
export const GET_DATA_LAUNDRY_FAILED = 'GET_LAUNDRY_DATA_FAILED';

export const GET_FILTER_MULTIPLE_SELECT_BEGIN = 'GET_FILTER_SELECT_BEGIN';
export const GET_FILTER_MULTIPLE_SELECT_SUCCESS = 'GET_FILTER_SELECT_SUCCESS';
export const GET_FILTER_MULTIPLE_SELECT_FAILED = 'GET_FILTER_SELECT_FAILED';

export const GET_LAUNDRIES_BEGIN = 'GET_All_LAUNDRY_BEGIN';
export const GET_LAUNDRIES_SUCCESS = 'GET_All_LAUNDRY_SUCCESS';
export const GET_LAUNDRIES_FAILED = 'GET_All_LAUNDRY_FAILED';

export const POST_ORDER_BEGIN = 'POST_ORDER_BEGIN';
export const POST_ORDER_SUCCESS = 'POST_ORDER_SUCCESS';
export const POST_ORDER_FAILED = 'POST_ORDER_FAILED';

export const GET_ORDER_BEGIN = 'GET_ORDER_BEGIN';
export const GET_ORDER_SUCCESS = 'GET_ORDER_SUCCESS';
export const GET_ORDER_FAILED = 'GET_ORDER_FAILED';

export const SET_ORDER_VALUE_SUCCESS = 'SET_ORDER_VALUE_SUCCESS';

export const GET_FAVORITE_LAUNDRY_BEGIN = 'GET_FAVORITE_LAUNDRY_BEGIN';
export const GET_FAVORITE_LAUNDRY_SUCCESS = 'GET_FAVORITE_LAUNDRY_SUCCESS';
export const GET_FAVORITE_LAUNDRY_FAILED = 'GET_FAVORITE_LAUNDRY_FAILED';

export const POST_FAVORITE_LAUNDRY_BEGIN = 'POST_FAVORITE_LAUNDRY_BEGIN';
export const POST_FAVORITE_LAUNDRY_SUCCESS = 'POST_FAVORITE_LAUNDRY_SUCCESS';
export const POST_FAVORITE_LAUNDRY_FAILED = 'POST_FAVORITE_LAUNDRY_FAILED';

export const DEL_FAVORITE_LAUNDRY_BEGIN = 'DEL_FAVORITE_LAUNDRY_BEGIN';
export const DEL_FAVORITE_LAUNDRY_SUCCESS = 'DEL_FAVORITE_LAUNDRY_SUCCESS';
export const DEL_FAVORITE_LAUNDRY_FAILED = 'DEL_FAVORITE_LAUNDRY_FAILED';

export const GET_ORDER_FINISH_BEGIN = 'GET_ORDER_FINISH_BEGIN';
export const GET_ORDER_FINISH_SUCCESS = 'GET_ORDER_FINISH_SUCCESS';
export const GET_ORDER_FINISH_FAILED = 'GET_ORDER_FINISH_FAILED';
