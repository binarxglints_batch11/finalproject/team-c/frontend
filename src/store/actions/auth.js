import { POST_REGISTER_SUCCESS, POST_REGISTER_FAILED, POST_LOGIN_SUCCESS, POST_LOGIN_FAILED, LOGOUT, SET_MESSAGE } from './actionTypes';
import AuthService from '../services/auth.service';

export const register = (email, password, confirmpassword) => (dispatch) => {
  return AuthService.register(email, password, confirmpassword).then(
    (response) => {
      dispatch({
        type: POST_REGISTER_SUCCESS,
        // payload: response.data,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: response.data.message,
      });
      // toast.success('MY SUCCESS');

      return Promise.resolve();
    },
    (error) => {
      const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();

      dispatch({
        type: POST_REGISTER_FAILED,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const login = (email, password) => (dispatch) => {
  return AuthService.login(email, password).then(
    (data) => {
      dispatch({
        type: POST_LOGIN_SUCCESS,
        payload: { user: data },
      });

      return Promise.resolve();
    },
    (error) => {
      const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();

      dispatch({
        type: POST_LOGIN_FAILED,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const logout = () => (dispatch) => {
  localStorage.removeItem();
  // AuthService.logout();

  dispatch({
    type: LOGOUT,
  });
};

// export const Register = () => (dispatch) => {
//   dispatch({
//     type: POST_REGISTER_BEGIN,
//     loading: true,
//     error: null,
//   });
// };
