import { GET_LAUNDRIES_BEGIN, GET_LAUNDRIES_SUCCESS, GET_LAUNDRIES_FAILED } from './actionTypes';
import axios from 'axios';

export const getLaundries =
  ({ name = '', service = '', type = '', deliver = '', level = '', page = 1 }) =>
  (dispatch) => {
    dispatch({
      type: GET_LAUNDRIES_BEGIN,
      loading: true,
      error: null,
    });

    const API_LAUNDRIES = `https://washme.gabatch11.my.id/search?name=${name}&service=${service}&level=${level}&type=${type}&deliver=${deliver}&page=${page}`;

    const token = localStorage.getItem('token');

    axios
      .get(API_LAUNDRIES, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      })
      .then((res) => {
        dispatch({
          type: GET_LAUNDRIES_SUCCESS,
          loading: false,
          payload: res.data.data,
        });
      })

      .catch((err) =>
        dispatch({
          type: GET_LAUNDRIES_FAILED,
          loading: false,
          error: err,
        })
      );
  };
