import { DEL_FAVORITE_LAUNDRY_BEGIN, DEL_FAVORITE_LAUNDRY_SUCCESS, DEL_FAVORITE_LAUNDRY_FAILED } from './actionTypes';
import axios from 'axios';
import { GetFavoriteLaundry } from './getFavoriteLaundry';

export const DelFavoriteLaundry = (id) => (dispatch) => {
  dispatch({
    type: DEL_FAVORITE_LAUNDRY_BEGIN,
    loading: true,
    error: null,
  });

  const API_USER = `https://washme.gabatch11.my.id/favorite/${id}`;
  const token = localStorage.getItem('token');

	// console.log(`${API_USER} + ${token}`);
	axios
		.delete(API_USER, {
			headers: {
				"Access-Control-Allow-Origin": "*",
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token,
			},
		})
		.then((res) => {
			// console.log("del-fav", res);
            dispatch(GetFavoriteLaundry());
			dispatch({
				type: DEL_FAVORITE_LAUNDRY_SUCCESS,
				loading: false,
				payload: res.data.id,
			});
		})

    .catch((err) =>
      dispatch({
        type: DEL_FAVORITE_LAUNDRY_FAILED,
        loading: false,
        error: err,
      })
    );
};