import { POST_FAVORITE_LAUNDRY_BEGIN, POST_FAVORITE_LAUNDRY_SUCCESS, POST_FAVORITE_LAUNDRY_FAILED } from './actionTypes';
import axios from 'axios';
import { GetFavoriteLaundry } from './getFavoriteLaundry';

export const PostFavoriteLaundry = (id) => (dispatch) => {
  dispatch({
    type: POST_FAVORITE_LAUNDRY_BEGIN,
    loading: true,
    error: null,
  });

  const API_USER = `https://washme.gabatch11.my.id/favorite/add`;
  const token = localStorage.getItem('token');

	// console.log(`${API_USER} + ${token}`);
	axios
		.post(API_USER, id, {
			headers: {
				"Access-Control-Allow-Origin": "*",
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token,
			},
		})
		.then((res) => {
			// console.log("post-fav", res);
			dispatch(GetFavoriteLaundry());

			dispatch({
				type: POST_FAVORITE_LAUNDRY_SUCCESS,
				loading: false,
				payload: res.data.id,
			});
		})

    .catch((err) =>
      dispatch({
        type: POST_FAVORITE_LAUNDRY_FAILED,
        loading: false,
        error: err,
      })
    );
};
