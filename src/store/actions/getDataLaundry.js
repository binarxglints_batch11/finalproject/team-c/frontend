import { GET_DATA_LAUNDRY_BEGIN, GET_DATA_LAUNDRY_SUCCESS, GET_DATA_LAUNDRY_FAILED } from './actionTypes';
import axios from 'axios';

export const GetDataLaundry = (id) => (dispatch) => {
  dispatch({
    type: GET_DATA_LAUNDRY_BEGIN,
    loading: true,
    error: null,
  });

  const API_DATA_LAUNDRY = `https://washme.gabatch11.my.id/details/${id}?service=1,2,3,4,5,6`;
  const token = localStorage.getItem('token');

  axios
    .get(API_DATA_LAUNDRY, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
    .then((res) => {
      dispatch({
        type: GET_DATA_LAUNDRY_SUCCESS,
        loading: false,
        payload: res.data.data,
      });
    })

    .catch((err) =>
      dispatch({
        type: GET_DATA_LAUNDRY_FAILED,
        loading: false,
        error: err,
      })
    );
};
