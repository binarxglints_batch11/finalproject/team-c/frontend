import { GET_USER_PROFILE_BEGIN, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILED } from './actionTypes';
import axios from 'axios';

export const GetUserProfile = (userId) => (dispatch) => {
  dispatch({
    type: GET_USER_PROFILE_BEGIN,
    loading: true,
    error: null,
  });

  const API_USER = `https://washme.gabatch11.my.id/customer/${userId}`;
  const token = localStorage.getItem('token');

  axios
    .get(API_USER, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
    .then((res) => {
      dispatch({
        type: GET_USER_PROFILE_SUCCESS,
        loading: false,
        payload: res.data.data,
      });
    })

    .catch((err) =>
      dispatch({
        type: GET_USER_PROFILE_FAILED,
        loading: false,
        error: err,
      })
    );
};
