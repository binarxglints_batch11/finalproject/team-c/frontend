import { GET_FILTER_SELECT_BEGIN, GET_FILTER_SELECT_SUCCESS, GET_FILTER_SELECT_FAILED } from './actionTypes';
import axios from 'axios';

export const getFilterSelect = () => (dispatch) => {
  dispatch({
    type: GET_FILTER_SELECT_BEGIN,
    loading: true,
    error: null,
  });

  const API_ALL_FILTER_SELECT = `https://washme.gabatch11.my.id/services`;
  // const token = localStorage.getItem('token');

  axios
    .get(API_ALL_FILTER_SELECT, {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        // Authorization: 'Bearer ' + token,
      },
    })
    .then((res) => {
      // console.log('filter', res);
      dispatch({
        type: GET_FILTER_SELECT_SUCCESS,
        loading: false,
        payload: res.data.data,
      });
    })

    .catch((err) =>
      dispatch({
        type: GET_FILTER_SELECT_FAILED,
        loading: false,
        error: err,
      })
    );
};
