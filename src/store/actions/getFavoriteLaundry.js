import { GET_FAVORITE_LAUNDRY_BEGIN, GET_FAVORITE_LAUNDRY_SUCCESS, GET_FAVORITE_LAUNDRY_FAILED } from './actionTypes';
import axios from 'axios';

export const GetFavoriteLaundry = () => (dispatch) => {
  dispatch({
    type: GET_FAVORITE_LAUNDRY_BEGIN,
    loading: true,
    error: null,
  });

  const API_USER = `https://washme.gabatch11.my.id/favorite`;
  const token = localStorage.getItem('token');

	// console.log(`${API_USER} + ${token}`);
	axios
		.get(API_USER, {
			headers: {
				"Access-Control-Allow-Origin": "*",
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token,
			},
		})
		.then((res) => {
			console.log("get-fav", res);
			dispatch({
				type: GET_FAVORITE_LAUNDRY_SUCCESS,
				loading: false,
				payload: res.data.data,
			});
		})

    .catch((err) =>
      dispatch({
        type: GET_FAVORITE_LAUNDRY_FAILED,
        loading: false,
        error: err,
      })
    );
};
