import { POST_ORDER_BEGIN, POST_ORDER_SUCCESS, POST_ORDER_FAILED } from './actionTypes';
import axios from 'axios';

export const PostOrder = (detailOrder) => (dispatch) => {
  dispatch({
    type: POST_ORDER_BEGIN,
    loading: true,
    error: null,
  });

  const API_USER = `https://washme.gabatch11.my.id/order/create`;
  const token = localStorage.getItem('token');
  const body = detailOrder;
  var config = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
  };

	// console.log("post-detail", detailOrder);

	axios
		.post(API_USER, body, config)
		.then((res) => {
			// console.log("post", res);
			dispatch({
				type: POST_ORDER_SUCCESS,
				loading: false,
				payload: res.createOrder,
			});
		})

		.catch((err) =>
			dispatch({
				type: POST_ORDER_FAILED,
				loading: false,
				error: err,
			})
		);
};
