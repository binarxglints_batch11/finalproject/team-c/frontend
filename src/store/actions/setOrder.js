import { SET_ORDER_VALUE_SUCCESS } from "./actionTypes";

export const SetOrder = (orderValue) => {
	return (dispatch) => {
		dispatch({
			type: SET_ORDER_VALUE_SUCCESS,
			payload: orderValue,
		});
	};
};
