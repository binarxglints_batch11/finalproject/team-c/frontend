import {
	GET_ORDER_FINISH_BEGIN,
	GET_ORDER_FINISH_SUCCESS,
	GET_ORDER_FINISH_FAILED,
} from "./actionTypes";
import axios from "axios";

export const GetOrderFinish = () => (dispatch) => {
	dispatch({
		type: GET_ORDER_FINISH_BEGIN,
		loading: true,
		error: null,
	});

	const API_USER = `https://washme.gabatch11.my.id/order/statusfinish`;
	const token = localStorage.getItem('token');

	axios
		.get(API_USER, {
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token,
			},
		})
		.then((res) => {
			// console.log("get-finish", res);
			dispatch({
				type: GET_ORDER_FINISH_SUCCESS,
				loading: false,
				payload: res.data.data,
			});
		})

		.catch((err) =>
			dispatch({
				type: GET_ORDER_FINISH_FAILED,
				loading: false,
				error: err,
			})
		);
};