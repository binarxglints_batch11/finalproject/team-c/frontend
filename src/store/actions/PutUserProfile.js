import { PUT_USER_PROFILE_BEGIN, PUT_USER_PROFILE_SUCCESS, PUT_USER_PROFILE_FAILED } from './actionTypes';
import axios from 'axios';

export const PutUserProfile = (idUser, formData) => (dispatch) => {
  dispatch({
    type: PUT_USER_PROFILE_BEGIN,
    loading: true,
    error: null,
  });

  const id = idUser.toString();
  const API_USER = `https://washme.gabatch11.my.id/customer/${id}`;
  const token = localStorage.getItem('token');

	// console.log(`${API_USER} + ${token}`);
	axios
		.put(API_USER, formData, {
			headers: {
				"Access-Control-Allow-Origin": "*",
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token,
			},
		})
		.then((res) => {
			// console.log("put-data-user", res);
			dispatch({
				type: PUT_USER_PROFILE_SUCCESS,
				loading: false,
				payload: res.data.data,
			});
		})

		.catch((err) =>
			dispatch({
				type: PUT_USER_PROFILE_FAILED,
				loading: false,
				error: err,
			})
		);
};
