import { GET_ORDER_BEGIN, GET_ORDER_SUCCESS, GET_ORDER_FAILED } from './actionTypes';
import axios from 'axios';

export const GetOrder = () => (dispatch) => {
	dispatch({
		type: GET_ORDER_BEGIN,
		loading: true,
		error: null,
	});

	const API_USER = `https://washme.gabatch11.my.id/order/status`;
	const token = localStorage.getItem('token');

	axios
		.get(API_USER, {
			headers: {
				"Content-Type": "application/json",
				"Authorization": "Bearer " + token,
			},
		})
		.then((res) => {
			// console.log("get-order", res);
			dispatch({
				type: GET_ORDER_SUCCESS,
				loading: false,
				payload: res.data.data,
			});
		})

    .catch((err) =>
      dispatch({
        type: GET_ORDER_FAILED,
        loading: false,
        error: err,
      })
    );
};
