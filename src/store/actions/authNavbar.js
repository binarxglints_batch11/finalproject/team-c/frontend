import { SET_LOGIN_CHANGE, SET_REGISTER_CHANGE } from './actionTypes';

export const registerForm = (register) => {
  return (dispatch) => {
    dispatch({
      type: SET_REGISTER_CHANGE,
      payload: register,
    });
  };
};

export const loginForm = (login) => {
  return (dispatch) => {
    dispatch({
      type: SET_LOGIN_CHANGE,
      payload: login,
    });
  };
};
