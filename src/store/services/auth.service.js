import axios from 'axios';

const API_URL = 'https://washme.gabatch11.my.id/';

class AuthService {
  login(email, password) {
    return axios.post(API_URL + 'login/customer', { email, password }).then((response) => {
      if (response.data.token) {
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('id', response.data.id);
      }

      return response.data;
    });
  }

  register(email, password, confirmpassword) {
    return axios.post(API_URL + 'Register/customer', {
      email,
      password,
      confirmpassword,
    });
  }
}

// const verifyUser = (code) => {
//   return axios.get(API_URL + "confirm/" + code).then((response) => {
//     return response.data;
//   });

export default new AuthService();
