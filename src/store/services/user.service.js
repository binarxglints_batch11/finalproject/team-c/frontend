import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://washme.gabatch11.my.id/';

class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'customer', { headers: authHeader() });
  }
}

export default new UserService();
